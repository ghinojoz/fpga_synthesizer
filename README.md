## Project Structure
![schematic image](/schematic.PNG "schematic image")

The above image illustrates the schematic for the FPGA design with all of its major modules. The modules perform the following functions:
1. top.sv -- Found in synthesizer folder -- This is the master module which handles instantiation of all the other modules in the project, as well as taking
information from the FPGA pins. It also performs minimal control functions described below (+ and mux)
1. pulse_debounce.sv -- Found in synthesizer folder-- Debounces input and ensures that a high signal is only sent for 1 slowclock period
1. kypd_in.sv -- Found in synthesizer folder-- Detects user input given a PmodKYPD. Heavily inspired by the example code provided on the digilent Pmod webpage
1. multi_debounce.sv -- Found in synthesizer folder-- Debounces input coming from the PmodKYPD, required to properly record, parameterized to debounce input of different bit width
1. record48.sv -- Found in synthesizer folder-- Records user input with the following structure: {[input][counter]} with input being 16 bit length and counter being 32 bit length. Counter is used to define how long a key has been held or has been played in playback mode
1. frequency_select.sv -- Found in synthesizer folder -- Outputs a frequency as a 24 bit value based on user input, expects input to be coded by kypd_in.sv
1. _+_ -- Found in top.sv (in synthesizer folder) -- Frequencies from the recording and keys being played are added together before being passed to synthesizers. This may not be correct, I welcome feedback as my musical knowledge is limited
1. sine_oscillator.sv -- Found in synthesizer folder-- Generates a sine wave based on the input frequency using a LUT. The LUT is small due to space restrictions, and there is no interpolation between steps for the same reason. Expects a 44.1kHz sample clock.
1. square_oscillator.sv -- Found in synthesizer folder-- Generates a square wave based on the input frequency. Simply flips amplitude at appropriate intervals. Expects 44.1kHz sample clock
1. _mux_ -- Found in top.sv (in synthesizer folder) -- Based on the selected wave mode, either sine output or square output is sent to the output module.
1. axis_i2S2.v -- Found in provided modules folder -- expects a 25 mHz clock, generates a 44.1kHz sample clock. Note that the LINE IN on the pmod I2S2 is not handled **at all** If you wish to incorporate LINE IN, you will need to modify this file.

## Instructions
Instructions provided are for the iCEBreaker V1.0e
### Cloning
This repository can be cloned using the following instruction:
git clone https://git.ucsc.edu/ghinojoz/fpga_snythesizer
### Building
To build the project, you must have the following tools installed (note that this project was originally built on a linux machine. No guarantee is  given for other operating systems).
1. _Yosys:_ [https://yosyshq.net/yosys/](https://yosyshq.net/yosys/)(v0.23)
1. _nextpnr-ice40:_ [https://github.com/YosysHQ/nextpnr](https://github.com/YosysHQ/nextpnr) (v0.4)
1. _project-icestorm:_[https://clifford.at/icestorm](https://clifford.at/icestorm) (No Version)

### Testing
Under the /synthesizer directory, you can find a number of different modules with their own dedicated sub directory. These modules are the ones that have either a simulation of some kind or a testbench to ensure correct behavior. To test each of these modules, you can navigate to their respective sub directory and enter the command: _make test_ 
This process will check the module using verilator and iverlilog. Additionally, _make test_ will produce a iverilog.vcd file which can be viewed using GTKWAVE. To view this file, simply run: _make test_. Then, in the command line, enter the command _gtkwave iverilog.vcd_. It is likely that you will need to zoom out to be able to view the waveform! This can be achieved using the gtkwave GUI. If you are new to GTKWAVE you may find it help to look at the user manual, found here: [https://gtkwave.sourceforge.net/gtkwave.pdf](https://gtkwave.sourceforge.net/gtkwave.pdf)
### Implementation
In order to place the project into an FPGA, the following materials are required:
1. iCEBreaker v1.0e
1. PmodI2S2 [https://digilent.com/shop/pmod-i2s2-stereo-audio-input-and-output/](https://digilent.com/shop/pmod-i2s2-stereo-audio-input-and-output/)
1. PmodKYPD [https://digilent.com/shop/pmod-kypd-16-button-keypad/](https://digilent.com/shop/pmod-kypd-16-button-keypad/)
1. USB cable to connect iCEBreaker to pc
1. aux headphones or speakers

PmodI2S2 must be plugged into the PMOD1A pins on the iCEBreaker FPGA, and PmodKYPD must be plugged into the PMOD1B pins on the iCEBreaker FPGA.
Note that this is a tight fit. If you wish to change which pins the Pmods use, you must modify the icebreaker.pcf file in the _provided_modules_ directory

To build the bitstream and place it on the FPGA, ensure your FPGA is connected via USB to your machine, then navigate to the _synthesizer_ folder. From within that directory, run _make prog_ and the bit stream will be built and 
sent to the FPGA. Note that this does take a few minutes!

Once the bitstream is completed you can play your synthesizer! Be warned that the square oscillator is somewhat loud, **I recommend lowering the volume of your headphones or speaker** and gradually increasing the volume to an appropriate level.
To play the synthesizer, the keypad determines the note played, from C4 at keypad 0 up to D5# on keypad F. Note that currently, only one key may be pressed at a time.
Multiple key presses will result in no tone being produced. There are also 3 buttons on the fpga itself, which do the following:
1. Button1: Toggles record or playback mode
1. Button2: Select square wave output
1. Button2: Select sine wave output

A note about recording: Currently the module has enough memory to record up to 15 different elements with a duration of roughly 170 seconds. The recording module
records both pauses and key presses for as long as they are held. That said, if you play the following sequence: begin_recording - pause - kypd_1 - pause - kypd_2 - pause - end_recording
that will count as 5/15 elements, not 2. (remember spaces between keypresses are recroded as well). Upon exiting recording mode, the synthesizer will enter playback mode, repeatedly looping through
the recorded inputs. To stop this loop, you can press the record button to enable recording, and press the record button again to return to the playback mode. Note that this will result in previous recordings being lost.
All recordings are lost when the fpga enters record mode. The bright red LED 1 will shine while the FPGA is recording, it will be off when in playback mode.
LED 2 and LED 3 are used to denote square wave mode and sine wave mode respectively. Note that if the FPGA is in sine wave mode, all tones will be played in this mode, even if they were recorded while in square wave mode.


Have fun!

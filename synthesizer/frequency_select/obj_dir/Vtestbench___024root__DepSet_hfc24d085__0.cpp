// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtestbench.h for the primary calling header

#include "verilated.h"

#include "Vtestbench__Syms.h"
#include "Vtestbench___024root.h"

VL_INLINE_OPT VlCoroutine Vtestbench___024root___eval_initial__TOP__0(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial__TOP__0\n"); );
    // Init
    VlWide<4>/*127:0*/ __Vtemp_h9849538a__0;
    // Body
    __Vtemp_h9849538a__0[0U] = 0x2e667374U;
    __Vtemp_h9849538a__0[1U] = 0x61746f72U;
    __Vtemp_h9849538a__0[2U] = 0x6572696cU;
    __Vtemp_h9849538a__0[3U] = 0x76U;
    vlSymsp->_vm_contextp__->dumpfile(VL_CVT_PACK_STR_NW(4, __Vtemp_h9849538a__0));
    vlSymsp->_traceDumpOpen();
    VL_WRITEF("\n _____         _   _               _      _____                                       _____     _         _   \n|_   _|___ ___| |_| |_ ___ ___ ___| |_   |   __|___ ___ ___ _ _ ___ ___ ___ _ _      |   __|___| |___ ___| |_ \n  | | | -_|_ -|  _| . | -_|   |  _|   |  |   __|  _| -_| . | | | -_|   |  _| | |     |__   | -_| | -_|  _|  _|\n  |_| |___|___|_| |___|___|_|_|___|_|_|  |__|  |_| |___|_  |___|___|_|_|___|_  |_____|_____|___|_|___|___|_|  \n");
    VL_WRITEF("                                                         |_|               |___|_____|                        \n\nBegin Simulation:\n");
    vlSelf->testbench__DOT__itervar = 0U;
    vlSelf->testbench__DOT__error = 0U;
    co_await vlSelf->__VdlySched.delay(0x9c40U, "testbench.sv", 
                                       71);
    co_await vlSelf->__VdlySched.delay(0x2710U, "testbench.sv", 
                                       72);
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           0: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 1U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           1: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 2U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           2: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 3U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           3: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 4U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           4: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 5U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           5: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 6U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           6: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 7U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           7: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 8U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           8: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 9U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge           9: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 0xaU;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge          10: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 0xbU;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge          11: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 0xcU;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge          12: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 0xdU;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge          13: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 0xeU;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge          14: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 0xfU;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge          15: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 0x10U;
    co_await vlSelf->__VtrigSched_hef748430__0.trigger(
                                                       "@(posedge testbench.clk_i)", 
                                                       "testbench.sv", 
                                                       75);
    VL_WRITEF("At Posedge          16: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b\n",
              16,vlSelf->testbench__DOT__kypd_i,24,
              vlSelf->testbench__DOT__dut__DOT__frequency_r,
              24,vlSelf->testbench__DOT__test_freq_o);
    vlSelf->testbench__DOT__itervar = 0x11U;
    VL_FINISH_MT("testbench.sv", 78, "");
}

#ifdef VL_DEBUG
VL_ATTR_COLD void Vtestbench___024root___dump_triggers__act(Vtestbench___024root* vlSelf);
#endif  // VL_DEBUG

void Vtestbench___024root___eval_triggers__act(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_triggers__act\n"); );
    // Body
    vlSelf->__VactTriggered.at(0U) = ((~ (IData)(vlSelf->testbench__DOT__clk_i)) 
                                      & (IData)(vlSelf->__Vtrigrprev__TOP__testbench__DOT__clk_i));
    vlSelf->__VactTriggered.at(1U) = ((IData)(vlSelf->testbench__DOT__clk_i) 
                                      & (~ (IData)(vlSelf->__Vtrigrprev__TOP__testbench__DOT__clk_i)));
    vlSelf->__VactTriggered.at(2U) = vlSelf->__VdlySched.awaitingCurrentTime();
    vlSelf->__Vtrigrprev__TOP__testbench__DOT__clk_i 
        = vlSelf->testbench__DOT__clk_i;
#ifdef VL_DEBUG
    if (VL_UNLIKELY(vlSymsp->_vm_contextp__->debug())) {
        Vtestbench___024root___dump_triggers__act(vlSelf);
    }
#endif
}

VL_INLINE_OPT void Vtestbench___024root___nba_sequent__TOP__0(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___nba_sequent__TOP__0\n"); );
    // Body
    vlSelf->testbench__DOT__kypd_i = (0xffffU & (IData)(
                                                        (((0x11U 
                                                           >= 
                                                           (0x1fU 
                                                            & vlSelf->testbench__DOT__itervar))
                                                           ? 
                                                          vlSelf->testbench__DOT__test_vector
                                                          [
                                                          (0x1fU 
                                                           & vlSelf->testbench__DOT__itervar)]
                                                           : 0ULL) 
                                                         >> 0x18U)));
    if (VL_UNLIKELY(vlSelf->testbench__DOT__error_o)) {
        VL_WRITEF("[%0t] %%Error: testbench.sv:83: Assertion failed in %Ntestbench: \033[0;31mError!\033[0m: dut_freq_o should be %b, got %b\n",
                  64,VL_TIME_UNITED_Q(1000),-9,vlSymsp->name(),
                  24,vlSelf->testbench__DOT__test_freq_o,
                  24,vlSelf->testbench__DOT__dut__DOT__frequency_r);
        vlSelf->testbench__DOT__error = 1U;
        VL_STOP_MT("testbench.sv", 83, "");
    }
    vlSelf->testbench__DOT__test_freq_o = (0xffffffU 
                                           & (IData)(
                                                     ((0x11U 
                                                       >= 
                                                       (0x1fU 
                                                        & vlSelf->testbench__DOT__itervar))
                                                       ? 
                                                      vlSelf->testbench__DOT__test_vector
                                                      [
                                                      (0x1fU 
                                                       & vlSelf->testbench__DOT__itervar)]
                                                       : 0ULL)));
}

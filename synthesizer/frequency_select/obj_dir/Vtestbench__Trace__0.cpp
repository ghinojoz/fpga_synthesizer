// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_fst_c.h"
#include "Vtestbench__Syms.h"


void Vtestbench___024root__trace_chg_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp);

void Vtestbench___024root__trace_chg_top_0(void* voidSelf, VerilatedFst::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_chg_top_0\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    Vtestbench___024root__trace_chg_sub_0((&vlSymsp->TOP), bufp);
}

void Vtestbench___024root__trace_chg_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_chg_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode + 1);
    // Body
    if (VL_UNLIKELY(vlSelf->__Vm_traceActivity[1U])) {
        bufp->chgQData(oldp+0,(vlSelf->testbench__DOT__test_vector[0]),40);
        bufp->chgQData(oldp+2,(vlSelf->testbench__DOT__test_vector[1]),40);
        bufp->chgQData(oldp+4,(vlSelf->testbench__DOT__test_vector[2]),40);
        bufp->chgQData(oldp+6,(vlSelf->testbench__DOT__test_vector[3]),40);
        bufp->chgQData(oldp+8,(vlSelf->testbench__DOT__test_vector[4]),40);
        bufp->chgQData(oldp+10,(vlSelf->testbench__DOT__test_vector[5]),40);
        bufp->chgQData(oldp+12,(vlSelf->testbench__DOT__test_vector[6]),40);
        bufp->chgQData(oldp+14,(vlSelf->testbench__DOT__test_vector[7]),40);
        bufp->chgQData(oldp+16,(vlSelf->testbench__DOT__test_vector[8]),40);
        bufp->chgQData(oldp+18,(vlSelf->testbench__DOT__test_vector[9]),40);
        bufp->chgQData(oldp+20,(vlSelf->testbench__DOT__test_vector[10]),40);
        bufp->chgQData(oldp+22,(vlSelf->testbench__DOT__test_vector[11]),40);
        bufp->chgQData(oldp+24,(vlSelf->testbench__DOT__test_vector[12]),40);
        bufp->chgQData(oldp+26,(vlSelf->testbench__DOT__test_vector[13]),40);
        bufp->chgQData(oldp+28,(vlSelf->testbench__DOT__test_vector[14]),40);
        bufp->chgQData(oldp+30,(vlSelf->testbench__DOT__test_vector[15]),40);
        bufp->chgQData(oldp+32,(vlSelf->testbench__DOT__test_vector[16]),40);
        bufp->chgQData(oldp+34,(vlSelf->testbench__DOT__test_vector[17]),40);
    }
    bufp->chgBit(oldp+36,(vlSelf->testbench__DOT__clk_i));
    bufp->chgSData(oldp+37,(vlSelf->testbench__DOT__kypd_i),16);
    bufp->chgIData(oldp+38,(vlSelf->testbench__DOT__dut__DOT__frequency_r),24);
    bufp->chgIData(oldp+39,(vlSelf->testbench__DOT__test_freq_o),24);
    bufp->chgBit(oldp+40,(vlSelf->testbench__DOT__error));
    bufp->chgBit(oldp+41,((vlSelf->testbench__DOT__dut__DOT__frequency_r 
                           != vlSelf->testbench__DOT__test_freq_o)));
    bufp->chgIData(oldp+42,(vlSelf->testbench__DOT__itervar),32);
}

void Vtestbench___024root__trace_cleanup(void* voidSelf, VerilatedFst* /*unused*/) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_cleanup\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    vlSymsp->__Vm_activity = false;
    vlSymsp->TOP.__Vm_traceActivity[0U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[1U] = 0U;
}

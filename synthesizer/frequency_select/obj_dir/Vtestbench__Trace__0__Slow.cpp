// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_fst_c.h"
#include "Vtestbench__Syms.h"


VL_ATTR_COLD void Vtestbench___024root__trace_init_sub__TOP__0(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_init_sub__TOP__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->pushNamePrefix("testbench ");
    tracep->declBus(c+37,"clk_i",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+38,"kypd_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 15,0);
    tracep->declBus(c+39,"dut_freq_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 23,0);
    tracep->declBus(c+40,"test_freq_o",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 23,0);
    tracep->declBus(c+41,"error",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+42,"error_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+43,"itervar",-1, FST_VD_IMPLICIT,FST_VT_SV_INT, false,-1, 31,0);
    for (int i = 0; i < 18; ++i) {
        tracep->declQuad(c+1+i*2,"test_vector",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, true,(i+0), 39,0);
    }
    tracep->pushNamePrefix("cg ");
    tracep->declBus(c+44,"cycle_time_p",-1, FST_VD_IMPLICIT,FST_VT_VCD_PARAMETER, false,-1, 31,0);
    tracep->declBit(c+37,"clk_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("dut ");
    tracep->declBus(c+37,"clk_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+38,"kypd_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 15,0);
    tracep->declBus(c+39,"frequency_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 23,0);
    tracep->declBus(c+39,"frequency_r",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 23,0);
    tracep->popNamePrefix(2);
}

VL_ATTR_COLD void Vtestbench___024root__trace_init_top(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_init_top\n"); );
    // Body
    Vtestbench___024root__trace_init_sub__TOP__0(vlSelf, tracep);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_top_0(void* voidSelf, VerilatedFst::Buffer* bufp);
void Vtestbench___024root__trace_chg_top_0(void* voidSelf, VerilatedFst::Buffer* bufp);
void Vtestbench___024root__trace_cleanup(void* voidSelf, VerilatedFst* /*unused*/);

VL_ATTR_COLD void Vtestbench___024root__trace_register(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_register\n"); );
    // Body
    tracep->addFullCb(&Vtestbench___024root__trace_full_top_0, vlSelf);
    tracep->addChgCb(&Vtestbench___024root__trace_chg_top_0, vlSelf);
    tracep->addCleanupCb(&Vtestbench___024root__trace_cleanup, vlSelf);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp);

VL_ATTR_COLD void Vtestbench___024root__trace_full_top_0(void* voidSelf, VerilatedFst::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_full_top_0\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    Vtestbench___024root__trace_full_sub_0((&vlSymsp->TOP), bufp);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_full_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode);
    // Body
    bufp->fullQData(oldp+1,(vlSelf->testbench__DOT__test_vector[0]),40);
    bufp->fullQData(oldp+3,(vlSelf->testbench__DOT__test_vector[1]),40);
    bufp->fullQData(oldp+5,(vlSelf->testbench__DOT__test_vector[2]),40);
    bufp->fullQData(oldp+7,(vlSelf->testbench__DOT__test_vector[3]),40);
    bufp->fullQData(oldp+9,(vlSelf->testbench__DOT__test_vector[4]),40);
    bufp->fullQData(oldp+11,(vlSelf->testbench__DOT__test_vector[5]),40);
    bufp->fullQData(oldp+13,(vlSelf->testbench__DOT__test_vector[6]),40);
    bufp->fullQData(oldp+15,(vlSelf->testbench__DOT__test_vector[7]),40);
    bufp->fullQData(oldp+17,(vlSelf->testbench__DOT__test_vector[8]),40);
    bufp->fullQData(oldp+19,(vlSelf->testbench__DOT__test_vector[9]),40);
    bufp->fullQData(oldp+21,(vlSelf->testbench__DOT__test_vector[10]),40);
    bufp->fullQData(oldp+23,(vlSelf->testbench__DOT__test_vector[11]),40);
    bufp->fullQData(oldp+25,(vlSelf->testbench__DOT__test_vector[12]),40);
    bufp->fullQData(oldp+27,(vlSelf->testbench__DOT__test_vector[13]),40);
    bufp->fullQData(oldp+29,(vlSelf->testbench__DOT__test_vector[14]),40);
    bufp->fullQData(oldp+31,(vlSelf->testbench__DOT__test_vector[15]),40);
    bufp->fullQData(oldp+33,(vlSelf->testbench__DOT__test_vector[16]),40);
    bufp->fullQData(oldp+35,(vlSelf->testbench__DOT__test_vector[17]),40);
    bufp->fullBit(oldp+37,(vlSelf->testbench__DOT__clk_i));
    bufp->fullSData(oldp+38,(vlSelf->testbench__DOT__kypd_i),16);
    bufp->fullIData(oldp+39,(vlSelf->testbench__DOT__dut__DOT__frequency_r),24);
    bufp->fullIData(oldp+40,(vlSelf->testbench__DOT__test_freq_o),24);
    bufp->fullBit(oldp+41,(vlSelf->testbench__DOT__error));
    bufp->fullBit(oldp+42,((vlSelf->testbench__DOT__dut__DOT__frequency_r 
                            != vlSelf->testbench__DOT__test_freq_o)));
    bufp->fullIData(oldp+43,(vlSelf->testbench__DOT__itervar),32);
    bufp->fullIData(oldp+44,(0xaU),32);
}

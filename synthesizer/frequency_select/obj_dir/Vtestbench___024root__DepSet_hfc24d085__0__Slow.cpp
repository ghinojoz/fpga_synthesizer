// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtestbench.h for the primary calling header

#include "verilated.h"

#include "Vtestbench__Syms.h"
#include "Vtestbench___024root.h"

VL_ATTR_COLD void Vtestbench___024root___eval_initial__TOP(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial__TOP\n"); );
    // Body
    vlSelf->testbench__DOT__test_vector[0U] = 0ULL;
    vlSelf->testbench__DOT__test_vector[1U] = 0x1000106ULL;
    vlSelf->testbench__DOT__test_vector[2U] = 0x2000115ULL;
    vlSelf->testbench__DOT__test_vector[3U] = 0x4000126ULL;
    vlSelf->testbench__DOT__test_vector[4U] = 0x8000137ULL;
    vlSelf->testbench__DOT__test_vector[5U] = 0x1000014aULL;
    vlSelf->testbench__DOT__test_vector[6U] = 0x2000015dULL;
    vlSelf->testbench__DOT__test_vector[7U] = 0x40000172ULL;
    vlSelf->testbench__DOT__test_vector[8U] = 0x80000188ULL;
    vlSelf->testbench__DOT__test_vector[9U] = 0x10000019fULL;
    vlSelf->testbench__DOT__test_vector[0xaU] = 0x2000001b8ULL;
    vlSelf->testbench__DOT__test_vector[0xbU] = 0x4000001d2ULL;
    vlSelf->testbench__DOT__test_vector[0xcU] = 0x8000001eeULL;
    vlSelf->testbench__DOT__test_vector[0xdU] = 0x100000020bULL;
    vlSelf->testbench__DOT__test_vector[0xeU] = 0x200000022aULL;
    vlSelf->testbench__DOT__test_vector[0xfU] = 0x400000024bULL;
    vlSelf->testbench__DOT__test_vector[0x10U] = 0x800000026eULL;
    vlSelf->testbench__DOT__test_vector[0x11U] = 0xffff000000ULL;
    VL_WRITEF("%Ntestbench.cg with cycle_time_p          10\n",
              vlSymsp->name());
}

#ifdef VL_DEBUG
VL_ATTR_COLD void Vtestbench___024root___dump_triggers__stl(Vtestbench___024root* vlSelf);
#endif  // VL_DEBUG

VL_ATTR_COLD void Vtestbench___024root___eval_triggers__stl(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_triggers__stl\n"); );
    // Body
    vlSelf->__VstlTriggered.at(0U) = (0U == vlSelf->__VstlIterCount);
#ifdef VL_DEBUG
    if (VL_UNLIKELY(vlSymsp->_vm_contextp__->debug())) {
        Vtestbench___024root___dump_triggers__stl(vlSelf);
    }
#endif
}

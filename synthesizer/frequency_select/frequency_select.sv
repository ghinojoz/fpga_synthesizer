module frequency_select
	(input [0:0] clk_i
	,input [15:0] kypd_i
	,output [23:0] frequency_o
	);
	
	logic [23:0] frequency_r; 
	
	always_ff @(posedge clk_i) begin
		case(kypd_i)
			16'b0000000000000010: begin // kypd 1
			 	frequency_r <= 24'd277; // FREQUENCY C#
			end
			16'b0000000000010000: begin// kypd 4
				frequency_r <= 24'd330;// FREQUENCY E4
			end
			16'b0000000010000000: begin // kypd 7 
				frequency_r <= 24'd392; // FREQUENCY G4
			end
			16'b0000000000000001: begin // kypd 0
				frequency_r <= 24'd262; // FREQUENCY C4
			end
			16'b0000000000000100: begin // kypd 2
				frequency_r <= 24'd294;//FREQUENCY D4
			end
			16'b0000000000100000: begin // kypd 5
				frequency_r <= 24'd349;// FREQUENCY F4
			end
			16'b0000000100000000: begin //kypd 8
				frequency_r <= 24'd415; // FREQUENCY G#
			end
			16'b1000000000000000: begin // kypd F
				frequency_r <= 24'd622; // FREQUENCY D#
			end
			16'b0000000000001000: begin // kypd 3
				frequency_r <= 24'd311; //FREQUENCY D#
			end
			16'b000000001000000: begin //kypd 6
				frequency_r <= 24'd370; // FREQUENCY F#
			end
			16'b0000001000000000: begin //kypd 9
				frequency_r <= 24'd440; //FREQUENCY A4
			end
			16'b0100000000000000: begin //kypd E
				frequency_r <= 24'd587; // FREQUENCY D5
			end
			16'b0000010000000000: begin //kypd A
				frequency_r <= 24'd466; // FREQUENCY A#
			end
			16'b0000100000000000: begin //kypd B
				frequency_r <= 24'd494; // FREQUENCY B4
			end
			16'b0001000000000000: begin //kypd C
				frequency_r <= 24'd523; // FREQUENCY C5
			end
			16'b0010000000000000: begin //kypd D
				frequency_r <= 24'd554; // FREQUENCY C#
			end
			default: begin // multi input or no input
				frequency_r <= 24'd0; // hopefully no sound
			end
		endcase
	end
	
	assign frequency_o = frequency_r;
endmodule

`timescale 1ns/1ps
module testbench();
	
	wire [0:0] clk_i;
	//wire [0:0] reset_i;
	logic [15:0] kypd_i;
	wire [23:0] dut_freq_o;
	logic [23:0] test_freq_o = 24'b000000000000000000000000;
	logic [0:0] error;
	wire [0:0] error_o;
	
	int itervar;
	
	assign error_o = (dut_freq_o !== test_freq_o);
	
	nonsynth_clock_gen
		#(.cycle_time_p(10))
	cg
		(.clk_o(clk_i));
		
	logic [39:0] test_vector [17:0];
	initial begin
		//msb is input, lsb is output freq
		test_vector[0] = {16'b0000000000000000, 24'd0};
		test_vector[1] = {16'b0000000000000001, 24'd262};
		test_vector[2] = {16'b0000000000000010, 24'd277};
		test_vector[3] = {16'b0000000000000100, 24'd294};
		test_vector[4] = {16'b0000000000001000, 24'd311};
		test_vector[5] = {16'b0000000000010000, 24'd330};
		test_vector[6] = {16'b0000000000100000, 24'd349};
		test_vector[7] = {16'b0000000001000000, 24'd370};
		test_vector[8] = {16'b0000000010000000, 24'd392};
		test_vector[9] = {16'b0000000100000000, 24'd415};
		test_vector[10] = {16'b0000001000000000, 24'd440};
		test_vector[11] = {16'b0000010000000000, 24'd466};
		test_vector[12] = {16'b0000100000000000, 24'd494};
		test_vector[13] = {16'b0001000000000000, 24'd523};
		test_vector[14] = {16'b0010000000000000, 24'd554};
		test_vector[15] = {16'b0100000000000000, 24'd587};
		test_vector[16] = {16'b1000000000000000, 24'd622};
		test_vector[17] = {16'b1111111111111111, 24'd0};
	end
	
	frequency_select
		#()
	dut
	(.clk_i(clk_i)
	,.kypd_i(kypd_i)
	,.frequency_o(dut_freq_o)
	);
	
	initial begin
`ifdef VERILATOR
		$dumpfile("verilator.fst");
`else
		$dumpfile("iverilog.vcd");
`endif
		$dumpvars;
		
		$display();
		$display(" _____         _   _               _      _____                                       _____     _         _   ");
		$display("|_   _|___ ___| |_| |_ ___ ___ ___| |_   |   __|___ ___ ___ _ _ ___ ___ ___ _ _      |   __|___| |___ ___| |_ ");
		$display("  | | | -_|_ -|  _| . | -_|   |  _|   |  |   __|  _| -_| . | | | -_|   |  _| | |     |__   | -_| | -_|  _|  _|");
		$display("  |_| |___|___|_| |___|___|_|_|___|_|_|  |__|  |_| |___|_  |___|___|_|_|___|_  |_____|_____|___|_|___|___|_|  ");
		$display("                                                         |_|               |___|_____|                        ");
		$display();
		$display("Begin Simulation:");
		
		itervar = 0;
		error = 0;
		#40;
		#10;
		
		for (itervar = 0; itervar < 17; itervar++) begin
			@(posedge clk_i);
			$display("At Posedge %d: kypd_i = %b, dut_freq_o = %b, test_freq_o = %b", itervar, kypd_i, dut_freq_o, test_freq_o);
		end
		$finish();
	end
	
	always @(negedge clk_i) begin
		if (error_o) begin
			$error("\033[0;31mError!\033[0m: dut_freq_o should be %b, got %b",test_freq_o, dut_freq_o);
			error = 1'b1;
		end
	
		kypd_i = test_vector[itervar][39:24];
		test_freq_o = test_vector[itervar][23:0];
	end
	
	final begin
		$display("Simulation time is %t", $time);
		if(error) begin
			$display("\033[0;31m _____                 \033[0m");
			$display("\033[0;31m|   __|___ ___ ___ ___ \033[0m");
			$display("\033[0;31m|   __|  _|  _| . |  _|\033[0m");
			$display("\033[0;31m|_____|_| |_| |___|_|  \033[0m");
			$display("\033[0;31m                       \033[0m");
			$display();
			$display("Simulation Failed");

		end else begin
			$display("\033[0;32m                   __ \033[0m");
			$display("\033[0;32m _____            |  |\033[0m");
			$display("\033[0;32m|  _  |___ ___ ___|  |\033[0m");
			$display("\033[0;32m|   __| .'|_ -|_ -|__|\033[0m");
			$display("\033[0;32m|__|  |__,|___|___|__|\033[0m");
			$display();
			$display("Simulation Succeeded!");
		end
	end
endmodule

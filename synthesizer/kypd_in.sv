//with help from digilent.com/reference/pmod/pmodkypd example projects

module kypd_in
	(input clk_i
	,input [3:0] row_i
	,output [3:0] col_o
	//,output [7:0] io_o);
	,output [15:0] io_o);
	logic [19:0] sclk;
	logic [15:0] in_l;
	logic [3:0] col_l;
	
	initial begin
		sclk <= 20'b00000000000000000000;
		in_l <= 15'b000000000000000;
	end
	
	always_ff @(posedge clk_i) begin
		//counted 1ms
		if (sclk == 20'b00011000011010100000) begin
			//check col 1
			col_l <= 4'b0111;
			sclk <= sclk + 1'b1;
		end
		//is a row pushed?
		else if (sclk == 20'b00011000011010101000) begin
			case(row_i)
				4'b0111: begin
					in_l[1] <= 1'b1;
					in_l[4] <= 1'b0;
					in_l[7] <= 1'b0;
					in_l[0] <= 1'b0;
				end
				4'b1011: begin
					in_l[1] <= 1'b0;
					in_l[4] <= 1'b1;
					in_l[7] <= 1'b0;
					in_l[0] <= 1'b0;
				end
				4'b1101: begin
					in_l[1] <= 1'b0;
					in_l[4] <= 1'b0;
					in_l[7] <= 1'b1;
					in_l[0] <= 1'b0;
				end
				4'b1110: begin
					in_l[1] <= 1'b0;
					in_l[4] <= 1'b0;
					in_l[7] <= 1'b0;
					in_l[0] <= 1'b1;
				end
				default: begin
					in_l[1] <= 1'b0;
					in_l[4] <= 1'b0;
					in_l[7] <= 1'b0;
					in_l[0] <= 1'b0;
				end//do nothing
			endcase
			
			sclk <= sclk + 1'b1;
		end
		//counted 2ms
		else if(sclk == 20'b00110000110101000000) begin
			// check col2
			col_l <= 4'b1011;
			sclk <= sclk + 1'b1;
		end
		//check if a row is pushed
		else if (sclk == 20'b00110000110101001000) begin
			case(row_i)
				4'b0111: begin
					in_l[2] <= 1'b1;
					in_l[5] <= 1'b0;
					in_l[8] <= 1'b0;
					in_l[15] <= 1'b0;
				end
				4'b1011: begin
					in_l[2] <= 1'b0;
					in_l[5] <= 1'b1;
					in_l[8] <= 1'b0;
					in_l[15] <= 1'b0;
				end
				4'b1101: begin
					in_l[2] <= 1'b0;
					in_l[5] <= 1'b0;
					in_l[8] <= 1'b1;
					in_l[15] <= 1'b0;
				end
				4'b1110: begin
					in_l[2] <= 1'b0;
					in_l[5] <= 1'b0;
					in_l[8] <= 1'b0;
					in_l[15] <= 1'b1;
				end
				default: begin
					in_l[2] <= 1'b0;
					in_l[5] <= 1'b0;
					in_l[8] <= 1'b0;
					in_l[15] <= 1'b0;
				end//do nothing;
			endcase
			
			sclk <= sclk + 1'b1;
		end
		//counted 3ms
		else if (sclk == 20'b01001001001111100000) begin
			col_l <= 4'b1101;
			sclk <= sclk + 1'b1;
		end
		//check if a row is pushed
		else if (sclk == 20'b01001001001111101000) begin
			case(row_i)
				4'b0111: begin
					in_l[3] <= 1'b1;
					in_l[6] <= 1'b0;
					in_l[9] <= 1'b0;
					in_l[14] <= 1'b0;
				end
				4'b1011: begin
					in_l[3] <= 1'b0;
					in_l[6] <= 1'b1;
					in_l[9] <= 1'b0;
					in_l[14] <= 1'b0;
				end
				4'b1101: begin
					in_l[3] <= 1'b0;
					in_l[6] <= 1'b0;
					in_l[9] <= 1'b1;
					in_l[14] <= 1'b0;
				end
				4'b1110: begin
					in_l[3] <= 1'b0;
					in_l[6] <= 1'b0;
					in_l[9] <= 1'b0;
					in_l[14] <= 1'b1;
				end
				default: begin
					in_l[3] <= 1'b0;
					in_l[6] <= 1'b0;
					in_l[9] <= 1'b0;
					in_l[14] <= 1'b0;//do nothing;
				end
			endcase
			sclk <= sclk + 1'b1;
		end
		//counted 4ms
		else if (sclk == 20'b01100001101010000000) begin
			//col4
			col_l <= 4'b1110;
			sclk <= sclk + 1'b1;
		end
		//check if a row is pushed
		else if (sclk == 20'b01100001101010001000) begin
			case(row_i)
				4'b0111: begin
					in_l[10] <= 1'b1;
					in_l[11] <= 1'b0;
					in_l[12] <= 1'b0;
					in_l[13] <= 1'b0;
				end
				4'b1011: begin
					in_l[10] <= 1'b0;
					in_l[11] <= 1'b1;
					in_l[12] <= 1'b0;
					in_l[13] <= 1'b0;
				end
				4'b1101: begin
					in_l[10] <= 1'b0;
					in_l[11] <= 1'b0;
					in_l[12] <= 1'b1;
					in_l[13] <= 1'b0;
				end
				4'b1110: begin
					in_l[10] <= 1'b0;
					in_l[11] <= 1'b0;
					in_l[12] <= 1'b0;
					in_l[13] <= 1'b1;
				end
				default: begin
					in_l[10] <= 1'b0;
					in_l[11] <= 1'b0;
					in_l[12] <= 1'b0;
					in_l[13] <= 1'b0;
				end//do nothing;
			endcase
			sclk <= 20'b00000000000000000000;
		end
		else begin
			sclk <= sclk + 1'b1;
		end
	end
	
	assign io_o = in_l;
	assign col_o = col_l;
	
endmodule

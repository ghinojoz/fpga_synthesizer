	module square_oscillator
		(input [23:0] frequency_i
		,input [0:0] clk_i // this should be the sample clock I think
		,output [31:0] signal_o);
	
		logic [31:0] signal_l;
		logic [31:0] count_l;
		logic [31:0] switch_period_l;
	
		initial begin
			signal_l = 32'b00000000000011111111111111111111; // number of 1's determines sound volume!
			count_l = '0;
		end
	
		always_ff @(posedge clk_i) begin
			//invert signal at end of period
			if(count_l >= switch_period_l) begin
				signal_l <= ~signal_l;
				count_l <= '0;
			end else begin
				count_l <= count_l + 1'b1;
			end
		end
	
	//switch_period_l determined by sample clock and desired freq
		always_comb begin
		/*verilator lint_off WIDTH */
			switch_period_l = 8 * 44100 / (frequency_i); // 44.1kHz sample clock from axis modules, scalar like sine wave?
		/*verilator lint_off WIDTH */
		end
		
		assign signal_o = signal_l;
	endmodule

module sine_oscillator
	(input [23:0] frequency_i
	,input [0:0] clk_i // sample clock, samples at 44.10 kHz
	,output [31:0] signal_o);
	
	logic [31:0] sine_lut [31:0];
	logic [31:0] counter;
	logic [31:0] lut_step_freq_l; 
	logic [4:0] lut_index_l; 
	logic [31:0] signal_l;
	
	initial begin
	 //a larger lut gives me a 'failed to expand region' error
		sine_lut[0] = 32'h80000;
		sine_lut[1] = 32'h98f8b;
		sine_lut[2] = 32'hb0fbc;
		sine_lut[3] = 32'hc71ce;
		sine_lut[4] = 32'hda827;
		sine_lut[5] = 32'hea6d9;
		sine_lut[6] = 32'hf641a;
		sine_lut[7] = 32'hfd8a5;
		sine_lut[8] = 32'hfffff;
		sine_lut[9] = 32'hfd8a5;
		
		sine_lut[10] = 32'hf641a;
		sine_lut[11] = 32'hea6d9;
		sine_lut[12] = 32'hda827;
		sine_lut[13] = 32'hc71ce;
		sine_lut[14] = 32'hb0fbc;
		sine_lut[15] = 32'h98f8b;
		sine_lut[16] = 32'h80000;
		sine_lut[17] = 32'h67074;
		sine_lut[18] = 32'h4f043;
		sine_lut[19] = 32'h38e31;
		
		sine_lut[20] = 32'h257d8;
		sine_lut[21] = 32'h15926;
		sine_lut[22] = 32'h09be5;
		sine_lut[23] = 32'h0275a;
		sine_lut[24] = 32'h00000;
		sine_lut[25] = 32'h0275a;
		sine_lut[26] = 32'h09be5;
		sine_lut[27] = 32'h15926;
		sine_lut[28] = 32'h257d8;
		sine_lut[29] = 32'h38e31;
		
		sine_lut[30] = 32'h4f043;
		sine_lut[31] = 32'h67074;
	
		counter = 0;
	end
	
	//recalculate step frequency when frequency changes
	always_comb begin
		//scalar needed to slow down how quickly we step through lut, otherwise cannot hear the output
		lut_step_freq_l = ((8*44100) / (32 * frequency_i)); // scalar*sample freq / (lut_size)
	end
	
	//lets count our way through the lut
	always_ff @(posedge clk_i)begin
		if (counter >= lut_step_freq_l) begin
			//need to increment the step
			if (lut_index_l == 5'b11111) begin
				lut_index_l <= 5'b00000;
				counter <= '0;
			end else begin
				lut_index_l <= lut_index_l + 1'b1;
				counter <= '0;
			end
		end else begin
			counter <= counter + 1'b1;
		end
		signal_l <= sine_lut[lut_index_l];
	end

	assign signal_o = signal_l;
endmodule

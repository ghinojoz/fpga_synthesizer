// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_fst_c.h"
#include "Vtestbench__Syms.h"


void Vtestbench___024root__trace_chg_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp);

void Vtestbench___024root__trace_chg_top_0(void* voidSelf, VerilatedFst::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_chg_top_0\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    Vtestbench___024root__trace_chg_sub_0((&vlSymsp->TOP), bufp);
}

void Vtestbench___024root__trace_chg_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_chg_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode + 1);
    // Body
    if (VL_UNLIKELY(vlSelf->__Vm_traceActivity[1U])) {
        bufp->chgIData(oldp+0,(vlSelf->testbench__DOT__dut__DOT__sine_lut[0]),32);
        bufp->chgIData(oldp+1,(vlSelf->testbench__DOT__dut__DOT__sine_lut[1]),32);
        bufp->chgIData(oldp+2,(vlSelf->testbench__DOT__dut__DOT__sine_lut[2]),32);
        bufp->chgIData(oldp+3,(vlSelf->testbench__DOT__dut__DOT__sine_lut[3]),32);
        bufp->chgIData(oldp+4,(vlSelf->testbench__DOT__dut__DOT__sine_lut[4]),32);
        bufp->chgIData(oldp+5,(vlSelf->testbench__DOT__dut__DOT__sine_lut[5]),32);
        bufp->chgIData(oldp+6,(vlSelf->testbench__DOT__dut__DOT__sine_lut[6]),32);
        bufp->chgIData(oldp+7,(vlSelf->testbench__DOT__dut__DOT__sine_lut[7]),32);
        bufp->chgIData(oldp+8,(vlSelf->testbench__DOT__dut__DOT__sine_lut[8]),32);
        bufp->chgIData(oldp+9,(vlSelf->testbench__DOT__dut__DOT__sine_lut[9]),32);
        bufp->chgIData(oldp+10,(vlSelf->testbench__DOT__dut__DOT__sine_lut[10]),32);
        bufp->chgIData(oldp+11,(vlSelf->testbench__DOT__dut__DOT__sine_lut[11]),32);
        bufp->chgIData(oldp+12,(vlSelf->testbench__DOT__dut__DOT__sine_lut[12]),32);
        bufp->chgIData(oldp+13,(vlSelf->testbench__DOT__dut__DOT__sine_lut[13]),32);
        bufp->chgIData(oldp+14,(vlSelf->testbench__DOT__dut__DOT__sine_lut[14]),32);
        bufp->chgIData(oldp+15,(vlSelf->testbench__DOT__dut__DOT__sine_lut[15]),32);
        bufp->chgIData(oldp+16,(vlSelf->testbench__DOT__dut__DOT__sine_lut[16]),32);
        bufp->chgIData(oldp+17,(vlSelf->testbench__DOT__dut__DOT__sine_lut[17]),32);
        bufp->chgIData(oldp+18,(vlSelf->testbench__DOT__dut__DOT__sine_lut[18]),32);
        bufp->chgIData(oldp+19,(vlSelf->testbench__DOT__dut__DOT__sine_lut[19]),32);
        bufp->chgIData(oldp+20,(vlSelf->testbench__DOT__dut__DOT__sine_lut[20]),32);
        bufp->chgIData(oldp+21,(vlSelf->testbench__DOT__dut__DOT__sine_lut[21]),32);
        bufp->chgIData(oldp+22,(vlSelf->testbench__DOT__dut__DOT__sine_lut[22]),32);
        bufp->chgIData(oldp+23,(vlSelf->testbench__DOT__dut__DOT__sine_lut[23]),32);
        bufp->chgIData(oldp+24,(vlSelf->testbench__DOT__dut__DOT__sine_lut[24]),32);
        bufp->chgIData(oldp+25,(vlSelf->testbench__DOT__dut__DOT__sine_lut[25]),32);
        bufp->chgIData(oldp+26,(vlSelf->testbench__DOT__dut__DOT__sine_lut[26]),32);
        bufp->chgIData(oldp+27,(vlSelf->testbench__DOT__dut__DOT__sine_lut[27]),32);
        bufp->chgIData(oldp+28,(vlSelf->testbench__DOT__dut__DOT__sine_lut[28]),32);
        bufp->chgIData(oldp+29,(vlSelf->testbench__DOT__dut__DOT__sine_lut[29]),32);
        bufp->chgIData(oldp+30,(vlSelf->testbench__DOT__dut__DOT__sine_lut[30]),32);
        bufp->chgIData(oldp+31,(vlSelf->testbench__DOT__dut__DOT__sine_lut[31]),32);
    }
    bufp->chgBit(oldp+32,(vlSelf->testbench__DOT__clk_i));
    bufp->chgIData(oldp+33,(vlSelf->testbench__DOT__frequency_i),24);
    bufp->chgIData(oldp+34,(vlSelf->testbench__DOT__dut__DOT__signal_l),32);
    bufp->chgIData(oldp+35,(vlSelf->testbench__DOT__dut__DOT__counter),32);
    bufp->chgIData(oldp+36,(VL_DIV_III(32, (IData)(0x56220U), 
                                       (vlSelf->testbench__DOT__frequency_i 
                                        << 5U))),32);
    bufp->chgCData(oldp+37,(vlSelf->testbench__DOT__dut__DOT__lut_index_l),5);
}

void Vtestbench___024root__trace_cleanup(void* voidSelf, VerilatedFst* /*unused*/) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_cleanup\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    vlSymsp->__Vm_activity = false;
    vlSymsp->TOP.__Vm_traceActivity[0U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[1U] = 0U;
}

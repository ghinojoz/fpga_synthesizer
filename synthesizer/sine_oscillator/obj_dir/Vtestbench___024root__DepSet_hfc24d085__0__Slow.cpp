// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtestbench.h for the primary calling header

#include "verilated.h"

#include "Vtestbench__Syms.h"
#include "Vtestbench___024root.h"

VL_ATTR_COLD void Vtestbench___024root___eval_initial__TOP(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial__TOP\n"); );
    // Body
    VL_WRITEF("%Ntestbench.cg with cycle_time_p          10\n",
              vlSymsp->name());
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0U] = 0x80000U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[1U] = 0x98f8bU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[2U] = 0xb0fbcU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[3U] = 0xc71ceU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[4U] = 0xda827U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[5U] = 0xea6d9U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[6U] = 0xf641aU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[7U] = 0xfd8a5U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[8U] = 0xfffffU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[9U] = 0xfd8a5U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0xaU] = 0xf641aU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0xbU] = 0xea6d9U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0xcU] = 0xda827U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0xdU] = 0xc71ceU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0xeU] = 0xb0fbcU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0xfU] = 0x98f8bU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x10U] = 0x80000U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x11U] = 0x67074U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x12U] = 0x4f043U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x13U] = 0x38e31U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x14U] = 0x257d8U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x15U] = 0x15926U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x16U] = 0x9be5U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x17U] = 0x275aU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x18U] = 0U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x19U] = 0x275aU;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x1aU] = 0x9be5U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x1bU] = 0x15926U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x1cU] = 0x257d8U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x1dU] = 0x38e31U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x1eU] = 0x4f043U;
    vlSelf->testbench__DOT__dut__DOT__sine_lut[0x1fU] = 0x67074U;
    vlSelf->testbench__DOT__dut__DOT__counter = 0U;
}

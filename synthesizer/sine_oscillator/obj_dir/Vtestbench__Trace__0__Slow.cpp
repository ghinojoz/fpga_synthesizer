// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_fst_c.h"
#include "Vtestbench__Syms.h"


VL_ATTR_COLD void Vtestbench___024root__trace_init_sub__TOP__0(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_init_sub__TOP__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->pushNamePrefix("testbench ");
    tracep->declBus(c+33,"clk_i",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+34,"frequency_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 23,0);
    tracep->declBus(c+35,"signal_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 31,0);
    tracep->pushNamePrefix("cg ");
    tracep->declBus(c+39,"cycle_time_p",-1, FST_VD_IMPLICIT,FST_VT_VCD_PARAMETER, false,-1, 31,0);
    tracep->declBit(c+33,"clk_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("dut ");
    tracep->declBus(c+34,"frequency_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 23,0);
    tracep->declBus(c+33,"clk_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+35,"signal_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 31,0);
    for (int i = 0; i < 32; ++i) {
        tracep->declBus(c+1+i*1,"sine_lut",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, true,(i+0), 31,0);
    }
    tracep->declBus(c+36,"counter",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 31,0);
    tracep->declBus(c+37,"lut_step_freq_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 31,0);
    tracep->declBus(c+38,"lut_index_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 4,0);
    tracep->declBus(c+35,"signal_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 31,0);
    tracep->popNamePrefix(2);
}

VL_ATTR_COLD void Vtestbench___024root__trace_init_top(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_init_top\n"); );
    // Body
    Vtestbench___024root__trace_init_sub__TOP__0(vlSelf, tracep);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_top_0(void* voidSelf, VerilatedFst::Buffer* bufp);
void Vtestbench___024root__trace_chg_top_0(void* voidSelf, VerilatedFst::Buffer* bufp);
void Vtestbench___024root__trace_cleanup(void* voidSelf, VerilatedFst* /*unused*/);

VL_ATTR_COLD void Vtestbench___024root__trace_register(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_register\n"); );
    // Body
    tracep->addFullCb(&Vtestbench___024root__trace_full_top_0, vlSelf);
    tracep->addChgCb(&Vtestbench___024root__trace_chg_top_0, vlSelf);
    tracep->addCleanupCb(&Vtestbench___024root__trace_cleanup, vlSelf);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp);

VL_ATTR_COLD void Vtestbench___024root__trace_full_top_0(void* voidSelf, VerilatedFst::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_full_top_0\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    Vtestbench___024root__trace_full_sub_0((&vlSymsp->TOP), bufp);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_full_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode);
    // Body
    bufp->fullIData(oldp+1,(vlSelf->testbench__DOT__dut__DOT__sine_lut[0]),32);
    bufp->fullIData(oldp+2,(vlSelf->testbench__DOT__dut__DOT__sine_lut[1]),32);
    bufp->fullIData(oldp+3,(vlSelf->testbench__DOT__dut__DOT__sine_lut[2]),32);
    bufp->fullIData(oldp+4,(vlSelf->testbench__DOT__dut__DOT__sine_lut[3]),32);
    bufp->fullIData(oldp+5,(vlSelf->testbench__DOT__dut__DOT__sine_lut[4]),32);
    bufp->fullIData(oldp+6,(vlSelf->testbench__DOT__dut__DOT__sine_lut[5]),32);
    bufp->fullIData(oldp+7,(vlSelf->testbench__DOT__dut__DOT__sine_lut[6]),32);
    bufp->fullIData(oldp+8,(vlSelf->testbench__DOT__dut__DOT__sine_lut[7]),32);
    bufp->fullIData(oldp+9,(vlSelf->testbench__DOT__dut__DOT__sine_lut[8]),32);
    bufp->fullIData(oldp+10,(vlSelf->testbench__DOT__dut__DOT__sine_lut[9]),32);
    bufp->fullIData(oldp+11,(vlSelf->testbench__DOT__dut__DOT__sine_lut[10]),32);
    bufp->fullIData(oldp+12,(vlSelf->testbench__DOT__dut__DOT__sine_lut[11]),32);
    bufp->fullIData(oldp+13,(vlSelf->testbench__DOT__dut__DOT__sine_lut[12]),32);
    bufp->fullIData(oldp+14,(vlSelf->testbench__DOT__dut__DOT__sine_lut[13]),32);
    bufp->fullIData(oldp+15,(vlSelf->testbench__DOT__dut__DOT__sine_lut[14]),32);
    bufp->fullIData(oldp+16,(vlSelf->testbench__DOT__dut__DOT__sine_lut[15]),32);
    bufp->fullIData(oldp+17,(vlSelf->testbench__DOT__dut__DOT__sine_lut[16]),32);
    bufp->fullIData(oldp+18,(vlSelf->testbench__DOT__dut__DOT__sine_lut[17]),32);
    bufp->fullIData(oldp+19,(vlSelf->testbench__DOT__dut__DOT__sine_lut[18]),32);
    bufp->fullIData(oldp+20,(vlSelf->testbench__DOT__dut__DOT__sine_lut[19]),32);
    bufp->fullIData(oldp+21,(vlSelf->testbench__DOT__dut__DOT__sine_lut[20]),32);
    bufp->fullIData(oldp+22,(vlSelf->testbench__DOT__dut__DOT__sine_lut[21]),32);
    bufp->fullIData(oldp+23,(vlSelf->testbench__DOT__dut__DOT__sine_lut[22]),32);
    bufp->fullIData(oldp+24,(vlSelf->testbench__DOT__dut__DOT__sine_lut[23]),32);
    bufp->fullIData(oldp+25,(vlSelf->testbench__DOT__dut__DOT__sine_lut[24]),32);
    bufp->fullIData(oldp+26,(vlSelf->testbench__DOT__dut__DOT__sine_lut[25]),32);
    bufp->fullIData(oldp+27,(vlSelf->testbench__DOT__dut__DOT__sine_lut[26]),32);
    bufp->fullIData(oldp+28,(vlSelf->testbench__DOT__dut__DOT__sine_lut[27]),32);
    bufp->fullIData(oldp+29,(vlSelf->testbench__DOT__dut__DOT__sine_lut[28]),32);
    bufp->fullIData(oldp+30,(vlSelf->testbench__DOT__dut__DOT__sine_lut[29]),32);
    bufp->fullIData(oldp+31,(vlSelf->testbench__DOT__dut__DOT__sine_lut[30]),32);
    bufp->fullIData(oldp+32,(vlSelf->testbench__DOT__dut__DOT__sine_lut[31]),32);
    bufp->fullBit(oldp+33,(vlSelf->testbench__DOT__clk_i));
    bufp->fullIData(oldp+34,(vlSelf->testbench__DOT__frequency_i),24);
    bufp->fullIData(oldp+35,(vlSelf->testbench__DOT__dut__DOT__signal_l),32);
    bufp->fullIData(oldp+36,(vlSelf->testbench__DOT__dut__DOT__counter),32);
    bufp->fullIData(oldp+37,(VL_DIV_III(32, (IData)(0x56220U), 
                                        (vlSelf->testbench__DOT__frequency_i 
                                         << 5U))),32);
    bufp->fullCData(oldp+38,(vlSelf->testbench__DOT__dut__DOT__lut_index_l),5);
    bufp->fullIData(oldp+39,(0xaU),32);
}

`timescale 1ns/1ps
module testbench();
	wire [0:0] clk_i;
	//wire [0:0] reset_i;
	logic [23:0] frequency_i;
	wire [31:0] signal_o;
	
	nonsynth_clock_gen
		#(.cycle_time_p(10))
	cg
		(.clk_o(clk_i));
		
	initial begin
		//dont think i have to do anything here
	end
	
	/* verilator lint_off WIDTH */
	sine_oscillator
		#()
	dut
	(.frequency_i(frequency_i)
	,.clk_i(clk_i)
	,.signal_o(signal_o)
	);
	/* verilator lint_on WIDTH */
	
	initial begin
`ifdef VERILATOR
		$dumpfile("verilator.fst");
`else
		$dumpfile("iverilog.vcd");
`endif
		$dumpvars;
		
		$display();
		$display(" _____ _           _     _   _                _                           _ _ _     _           ");
		$display("|   __|_|_____ _ _| |___| |_|_|___ ___    ___|_|___ ___       ___ ___ ___|_| | |___| |_ ___ ___ ");
		$display("|__   | |     | | | | .'|  _| | . |   |  |_ -| |   | -_|     | . |_ -|  _| | | | .'|  _| . |  _|");
		$display("|_____|_|_|_|_|___|_|__,|_| |_|___|_|_|  |___|_|_|_|___|_____|___|___|___|_|_|_|__,|_| |___|_|  ");
		$display("                                                       |_____|                                  ");
		$display();
		$display("Begin Simulation:");
		
		#1000;
		frequency_i = 24'd440;
		#1000;
		frequency_i = 24'd262;
		#1000;
		frequency_i = 24'd659;
		#1000;
		$finish();
	end
	
	final begin
		$display("Simulation time is %t", $time);
		$display("Simulation Complete!");
	end
	
endmodule

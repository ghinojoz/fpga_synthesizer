module dff_en
		#(parameter[0:0] reset_val_p = 1'b0)
	(input [0:0] clk_i
	,input [0:0] reset_i
	,input [0:0] clk_en
	,input [0:0] d_i
	,output [0:0] q_o
	);
	
	logic [0:0] q_r = 1'b0;
	
	always_ff @(posedge clk_i) begin
		if (reset_i) begin
			q_r <= reset_val_p;
		end else begin
			if (clk_en == 1'b1) begin
				q_r <= d_i;
			end
		end
	end
	
	assign q_o = q_r;
endmodule

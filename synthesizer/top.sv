// constructed with frequency information from pages.mtu.edu/~suits/notefreqs.html
module top
	(input [0:0] clk_12mhz_i
	,input [0:0] reset_n_async_unsafe_i
		// n: Negative Polarity (0 when pressed, 1 otherwise)
		// async: Not synchronized to clock
		// unsafe: Not De-Bounced
	,input [3:1] button_async_unsafe_i
		// async: Not synchronized to clock
		// unsafe: not De-Bounced
	,input [3:0] kpyd_row_i
	,output[3:0] kpyd_col_o
	
	//LINE OUT (GREEN)
	//Main Clock (for synchronization)
	,output tx_main_clk_o
	// Selects between L/R channels, but called a "clock"
  ,output tx_lr_clk_o
	// Data clock
  ,output tx_data_clk_o
 	// Output Data
  ,output tx_data_o

	// Line In (Blue)
  // Main clock (for synchronization)
  //,output rx_main_clk_o
  // Selects between L/R channels, but called a "clock"
  //,output rx_lr_clk_o
  // Data clock
  //,output rx_data_clk_o
  // Input data
  //,input  rx_data_i
	,output [5:1] led_o);
	
	wire reset_n_sync_r;
	wire reset_sync_r;
	wire reset_r; // use as reset signal
	
	// synchronizer flip flops
	dff
		#()
	sync_a
		(.clk_i(clk_o)
		,.reset_i(1'b0)
		,.d_i(reset_n_async_unsafe_i)
		,.q_o(reset_n_sync_r)
		);	
	
	inv
		#()
	inv
		(.a_i(reset_n_sync_r)
		,.b_o(reset_sync_r)
		);
		
	dff
		#()
	sync_b
		(.clk_i(clk_o)
		,.reset_i(1'b0)
		,.d_i(reset_sync_r)
		,.q_o(reset_r)
		);
		
	//attempt to move axis_clk into top module to confront
	// 'clk_12mhz_i can only drive PLL' error
	wire clk_o;
	
	 SB_PLL40_PAD 
    #(.FEEDBACK_PATH("SIMPLE")
     ,.PLLOUT_SELECT("GENCLK")
     ,.DIVR(4'b0000)
     ,.DIVF(7'b1000011)
     ,.DIVQ(3'b101)
     ,.FILTER_RANGE(3'b001)
     )
   pll_inst
     (.PACKAGEPIN(clk_12mhz_i)
     ,.PLLOUTCORE(clk_o)
     ,.RESETB(1'b1)
     ,.BYPASS(1'b0)
     );
     
   assign axis_clk = clk_o;
	
	//convert input into the last button pushed
	
	logic [15:0] kypd_in;
	logic [15:0] kypd_in_debounced;
	logic [0:0] should_record_l;
	logic [5:1] led_l = 5'b00000;
	logic [0:0] osc_type_l = 1'b0; // 0 is square osc, 1 is sine osc
	wire [0:0] button1_w;
	wire [0:0] button2_w;
	wire [0:0] button3_w;
	
	kypd_in
		#()
	kypd_in_inst
		(.clk_i(clk_o)
		,.row_i(kpyd_row_i)
		,.col_o(kpyd_col_o)
		,.io_o(kypd_in)
		);
	
	//pulse debounce button 1, record button
	pulse_debounce
		#()
	pulse_debounce_inst_b1
	(.clk_i(clk_o)
	,.reset_i(reset_r)
	,.signal_i(button_async_unsafe_i[1])
	,.signal_o(button1_w)
	);
	
	//pulse debounce button 2, select square osc button
	pulse_debounce
		#()
	pulse_debounce_inst_b2
	(.clk_i(clk_o)
	,.reset_i(reset_r)
	,.signal_i(button_async_unsafe_i[2])
	,.signal_o(button2_w)
	);
	
	//pulse debounce button 3, select sine osc button
	pulse_debounce
		#()
	pulse_debounce_inst_b3
	(.clk_i(clk_o)
	,.reset_i(reset_r)
	,.signal_i(button_async_unsafe_i[3])
	,.signal_o(button3_w)
	);
	
	//debounce kypd input
	multi_debounce
		#(.width_p(16))
	multi_debounce_inst
	(.clk_i(clk_o)
	,.reset_i(reset_r)
	,.signal_i(kypd_in)
	,.signal_o(kypd_in_debounced)
	);
	
	
	always_ff @(posedge clk_o) begin
		if(button2_w) begin
			osc_type_l <= 1'b0;
		end
		
		if (button3_w) begin
			osc_type_l <= 1'b1;
		end
	end
	
	always_comb begin
		led_l [1:1] = should_record_l;
		led_l [2:2] = osc_type_l;
		led_l [3:3] = ~osc_type_l;
	end
	
	assign led_o[5:1] = led_l[5:1];
	
	//this needs to be moved into a different module
	logic [23:0] frequency_l;
	
	frequency_select
		#()
	frequency_select_inst
	(.clk_i(clk_o)
	,.kypd_i(kypd_in_debounced)
	,.frequency_o(frequency_l)
	);
	
	logic [31:0] signal_l;
	logic [31:0] square_signal_l;
	logic [31:0] sine_signal_l;
	logic [0:0] axis_tx_valid_l = 1'b1;
	logic [0:0] axis_tx_ready_l;
	logic [0:0] last = 1'b0;
	logic [15:0] playback_l;
	logic [23:0] playback_freq_l;
	
	record48
		#()
	record48_inst
	(.clk_i(clk_o)
	,.mode_toggle(button1_w)
	,.data_i(kypd_in_debounced)
	,.data_o(playback_l)
	,.recording_o(should_record_l)
	);
	
	frequency_select
		#()
	frequency_select_playback_inst
	(.clk_i(clk_o)
	,.kypd_i(playback_l)
	,.frequency_o(playback_freq_l)
	);
	
	logic [23:0] added_freq_l;
	
	always_ff @(posedge clk_o) begin
		added_freq_l <= playback_freq_l + frequency_l;
		if (axis_tx_ready_l) begin
			last <= ~last;
		end
	end
	
	square_oscillator
		#()
	square_oscillator_inst
		(.frequency_i(added_freq_l)
		,.clk_i(tx_data_clk_o) // should be the sample clock
		,.signal_o(square_signal_l)
		);
		
	sine_oscillator
		#()
	sine_oscillator_inst
		(.frequency_i(added_freq_l)
		,.clk_i(tx_data_clk_o) // should be the sample clock
		,.signal_o(sine_signal_l)
		);
		
	assign signal_l = osc_type_l ? sine_signal_l : square_signal_l;
		
	axis_i2s2
		#()
	i2s2_inst
		(.axis_clk(axis_clk)
		,.axis_resetn(~reset_r)
		,.tx_axis_c_data(signal_l)
		,.tx_axis_c_valid(axis_tx_valid_l)
		,.tx_axis_c_ready(axis_tx_ready_l)
		,.tx_axis_c_last(last)
		,.tx_mclk(tx_main_clk_o)
		,.tx_lrck(tx_lr_clk_o)
		,.tx_sclk(tx_data_clk_o)
		,.tx_sdout(tx_data_o)
		);
		
	
	
	endmodule

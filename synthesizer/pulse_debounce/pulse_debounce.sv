module pulse_debounce
	(input [0:0] clk_i
  ,input [0:0] reset_i // positive-polarity, synchronous reset
  ,input [0:0] signal_i
  ,output [0:0] signal_o);
  
  logic [0:0] clk_en_l = 0;
  logic [7:0] counter = 0;
  logic [0:0] dff_out1_l;
  logic [0:0] dff_out2_l;
  logic [0:0] dff_out3_l;
  logic [0:0] dff_out3_n_l;
  
  always_ff @(posedge clk_i) begin
  	if (counter >= 8'b10000000) begin
  		clk_en_l <= 1'b1;
  		counter <= 0;
  	end else begin
  		counter <= counter + 1;
  		clk_en_l <= 1'b0;
  	end
  end
  
  dff_en
  	#()
  dff_en_inst1
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.clk_en(clk_en_l)
  ,.d_i(signal_i)
  ,.q_o(dff_out1_l)
  );
  
  dff_en
  	#()
  dff_en_inst2
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.clk_en(clk_en_l)
  ,.d_i(dff_out1_l)
  ,.q_o(dff_out2_l)
  );
  
  dff_en
  	#()
  dff_en_inst3
  (.clk_i(clk_i)
  ,.reset_i(reset_i)
  ,.clk_en(clk_en_l)
  //,.clk_en(1'b1)
  ,.d_i(dff_out2_l)
  ,.q_o(dff_out3_l)
  );
  assign dff_out3_n_l = ~dff_out3_l;
	
	assign signal_o = (dff_out2_l & dff_out3_n_l);
	
endmodule

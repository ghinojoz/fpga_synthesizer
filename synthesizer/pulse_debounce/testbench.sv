`timescale 1ns/1ps
module testbench();
	
	wire [0:0] clk_i;
	//wire [0:0] reset_i;
	logic [0:0] signal_i;
	wire [0:0] signal_o;
	
	nonsynth_clock_gen
		#(.cycle_time_p(10))
	cg
		(.clk_o(clk_i));
		
	initial begin
		//dont think i have to do anything here
	end
	
	/* verilator lint_off WIDTH */
	pulse_debounce
		#()
	dut
	(.clk_i(clk_i)
	,.reset_i(1'b0)
	,.signal_i(signal_i)
	,.signal_o(signal_o)
	);
	/* verilator lint_on WIDTH */
	
	initial begin
`ifdef VERILATOR
		$dumpfile("verilator.fst");
`else
		$dumpfile("iverilog.vcd");
`endif
		$dumpvars;
		
		$display();
		$display(" _____         _   _               _              _                 _     _                       ");
		$display("|_   _|___ ___| |_| |_ ___ ___ ___| |_    ___ _ _| |___ ___       _| |___| |_ ___ _ _ ___ ___ ___ ");
		$display("  | | | -_|_ -|  _| . | -_|   |  _|   |  | . | | | |_ -| -_|     | . | -_| . | . | | |   |  _| -_|");
		$display("  |_| |___|___|_| |___|___|_|_|___|_|_|  |  _|___|_|___|___|_____|___|___|___|___|___|_|_|___|___|");
		$display("                                         |_|               |_____|                                ");
		$display();
		$display("Begin Simulation:");
		
		#40;
		signal_i = 1'b0;
		#10;
		signal_i = 1'b1;
		#20
		signal_i = 1'b0;
		#20
		signal_i = 1'b1;
		#10
		signal_i = 1'b0;
		#20
		signal_i = 1'b1;
		#30
		signal_i = 1'b0;
		#40
		signal_i = 1'b1;
		#10000
		signal_i = 1'b0;
		$finish();
	end
	
	final begin
		$display("Simulation time is %t", $time);
		$display("Simulation Complete!");
	end
	
endmodule

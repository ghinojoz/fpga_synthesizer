`timescale 1ns/1ps
module testbench();
	
	wire [0:0] clk_i;
	//wire [0:0] reset_i;
	logic [15:0] signal_i;
	wire [15:0] signal_o;
	
	nonsynth_clock_gen
		#(.cycle_time_p(10))
	cg
		(.clk_o(clk_i));
		
	initial begin
		//dont think i have to do anything here
	end
	
	/* verilator lint_off WIDTH */
	multi_debounce
		#(.width_p(16))
	dut
	(.clk_i(clk_i)
	,.reset_i(1'b0)
	,.signal_i(signal_i)
	,.signal_o(signal_o)
	);
	/* verilator lint_on WIDTH */
	
	initial begin
`ifdef VERILATOR
		$dumpfile("verilator.fst");
`else
		$dumpfile("iverilog.vcd");
`endif
		$dumpvars;
		
		$display();
		$display(" _____         _   _               _                _ _   _         _     _                       ");
		$display("|_   _|___ ___| |_| |_ ___ ___ ___| |_    _____ _ _| | |_|_|      _| |___| |_ ___ _ _ ___ ___ ___ ");
		$display("  | | | -_|_ -|  _| . | -_|   |  _|   |  |     | | | |  _| |     | . | -_| . | . | | |   |  _| -_|");
		$display("  |_| |___|___|_| |___|___|_|_|___|_|_|  |_|_|_|___|_|_| |_|_____|___|___|___|___|___|_|_|___|___|");
		$display("                                                           |_____|                                ");
		$display();
		$display("Begin Simulation:");
		
		#20;
		signal_i = 16'b0000000000000000;
		#10;
		signal_i = 16'b0000000000000001;
		#10
		signal_i = 16'b0000000000000000;
		#10
		signal_i = 16'b0000000000000001;
		#10
		signal_i = 16'b0000000000000000;
		#20
		signal_i = 16'b0000000000000001;
		#10
		signal_i = 16'b0000000000000000;
		#20
		signal_i = 16'b0000000000000001;
		#10000
		signal_i = 16'b0000000000000000;
		$finish();
	end
	
	final begin
		$display("Simulation time is %t", $time);
		$display("Simulation Complete!");
	end
	
endmodule

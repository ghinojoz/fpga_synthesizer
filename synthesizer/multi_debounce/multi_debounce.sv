module multi_debounce
	#(parameter width_p = 1)
	(input [0:0] clk_i
  ,input [0:0] reset_i // positive-polarity, synchronous reset
  ,input [width_p-1:0] signal_i
  ,output [width_p-1:0] signal_o);
  
  logic [0:0] clk_en_l = 1'b0;
  logic [3:0] counter = 4'b0000;
  logic [width_p-1:0] multi_dff_out1_l;
  logic [width_p-1:0] multi_dff_out2_l;
  
  always_ff @(posedge clk_i) begin
		if (counter >= 4'b1000) begin
			clk_en_l <= 1'b1;
			counter <= 4'b0000;
		end else begin
			counter <= counter + 1'b1;
			clk_en_l <= 1'b0;
		end
  end
  
  multi_dff
		#(.width_p(width_p)
		,.reset_val_p(0))
	multi_dff_inst
	(.clk_i(clk_i)
	,.reset_i(reset_i)
	,.clk_en(clk_en_l)
	,.d_i(signal_i)
	,.q_o(multi_dff_out1_l)
	);
	
	  multi_dff
		#(.width_p(width_p)
		,.reset_val_p(0))
	multi_dff_inst2
	(.clk_i(clk_i)
	,.reset_i(reset_i)
	,.clk_en(clk_en_l)
	,.d_i(multi_dff_out1_l)
	,.q_o(multi_dff_out2_l)
	);
	
	assign signal_o = multi_dff_out2_l;
endmodule

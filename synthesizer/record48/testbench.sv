`timescale 1ns/1ps
module testbench();
	
	wire [0:0] clk_i;
	//wire [0:0] reset_i;
	logic [0:0] mode_toggle_i;
	logic [15:0] data_i;
	wire [15:0] data_o;
	wire [0:0] recording_o;
	
	int itervar;
	
	nonsynth_clock_gen
		#(.cycle_time_p(10))
	cg
		(.clk_o(clk_i));
		
	logic [16:0] test_vector [63:0];
	initial begin
		test_vector[0] = 17'b00000000000000000;
		test_vector[1] = 17'b00000000000000000;
		test_vector[2] = 17'b00000000000000000;
		test_vector[3] = 17'b10000000000000000;
		test_vector[4] = 17'b00000000000000000;
		test_vector[5] = 17'b00000000000000000;
		test_vector[6] = 17'b00000000000000000;
		test_vector[7] = 17'b00000000000000001;
		test_vector[8] = 17'b00000000000000001;
		test_vector[9] = 17'b00000000000000001;
		
		test_vector[10] = 17'b00000000000000001;
		test_vector[11] = 17'b00000000000000000;
		test_vector[12] = 17'b00000000000000000;
		test_vector[13] = 17'b00000000000000100;
		test_vector[14] = 17'b00000000000000100;
		test_vector[15] = 17'b00000000000000100;
		test_vector[16] = 17'b00000000000000100;
		test_vector[17] = 17'b00000000000000100;
		test_vector[18] = 17'b00000000000000000;
		test_vector[19] = 17'b00000000000000000;
		
		test_vector[20] = 17'b00000000000000000;
		test_vector[21] = 17'b00000000000000000;
		test_vector[22] = 17'b00000000000010000;
		test_vector[23] = 17'b00000000000010000;
		test_vector[24] = 17'b00000000000010000;
		test_vector[25] = 17'b00000000000010000;
		test_vector[26] = 17'b00000000000010000;
		test_vector[27] = 17'b00000000000010000;
		test_vector[28] = 17'b00000000000010000;
		test_vector[29] = 17'b00000000000000000;

		test_vector[30] = 17'b00000000000000000;
		test_vector[31] = 17'b00000000000000000;
		test_vector[32] = 17'b10000000000000000;
		test_vector[33] = 17'b00000000000000000;
		test_vector[34] = 17'b00000000000000000;
		test_vector[35] = 17'b00000000000000000;
		test_vector[36] = 17'b00000000000000000;
		test_vector[37] = 17'b00000000000000000;
		test_vector[38] = 17'b00000000000000000;
		test_vector[39] = 17'b00000000000000000;
		
		test_vector[40] = 17'b00000000000000000;
		test_vector[41] = 17'b00000000000000000;
		test_vector[42] = 17'b00000000000000000;
		test_vector[43] = 17'b00000000000000000;
		test_vector[44] = 17'b00000000000000000;
		test_vector[45] = 17'b00000000000000000;
		test_vector[46] = 17'b00000000000000000;
		test_vector[47] = 17'b00000000000000000;
		test_vector[48] = 17'b00000000000000000;
		test_vector[49] = 17'b00000000000000000;
		
		test_vector[50] = 17'b00000000000000000;
		test_vector[51] = 17'b00000000000000000;
		test_vector[52] = 17'b00000000000000000;
		test_vector[53] = 17'b00000000000000000;
		test_vector[54] = 17'b00000000000000000;
		test_vector[55] = 17'b00000000000000000;
		test_vector[56] = 17'b00000000000000000;
		test_vector[57] = 17'b00000000000000000;
		test_vector[58] = 17'b00000000000000000;
		test_vector[59] = 17'b00000000000000000;
		
		test_vector[60] = 17'b00000000000000000;
		test_vector[61] = 17'b00000000000000000;
		test_vector[62] = 17'b00000000000000000;
		test_vector[63] = 17'b00000000000000000;

	end
	
	/* verilator lint_off WIDTH */
	record48
		#()
	dut
	(.clk_i(clk_i)
	,.mode_toggle(mode_toggle_i)
	,.data_i(data_i)
	,.data_o(data_o)
	,.recording_o(recording_o)
	);
	/* verilator lint_on WIDTH */
	
	initial begin
`ifdef VERILATOR
		$dumpfile("verilator.fst");
`else
		$dumpfile("iverilog.vcd");
`endif
		$dumpvars;
		
		$display();
		$display(" _____         _   _               _      _____                   _ ___ ___ ");
		$display("|_   _|___ ___| |_| |_ ___ ___ ___| |_   | __  |___ ___ ___ ___ _| | | | . |");
		$display("  | | | -_|_ -|  _| . | -_|   |  _|   |  |    -| -_|  _| . |  _| . |_  | . |");
		$display("  |_| |___|___|_| |___|___|_|_|___|_|_|  |__|__|___|___|___|_| |___| |_|___|");
		$display("                                                                            ");
		$display();
		$display("Begin Simulation:");
		
		itervar = 0;
		#10;
		#10;
		
		for (itervar = 0; itervar < 63; itervar++) begin
			@(posedge clk_i);
			$display("At Posedge %d: recording_o = %b, data_o = %b", itervar, recording_o, data_o);
		end
		$finish();
	end
	
	always @(negedge clk_i) begin
		mode_toggle_i = test_vector[itervar][16];
		data_i = test_vector[itervar][15:0];
	end
	
	final begin
		$display("Simulation time is %t", $time);
		$display("Simulation Complete!");
	end
	
endmodule

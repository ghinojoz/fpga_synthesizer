//expects input to be debounced and synchronized!
module record48
	(input [0:0] clk_i
	,input [0:0] mode_toggle //controls whether or not the module should be in record or playbackmode
	,input [15:0] data_i
	,output [15:0] data_o
	,output [0:0] recording_o
	);
	
	logic [0:0] recording_l = 1'b0;
	logic [15:0] last_in_l;
	logic [31:0] counter;
	
	logic [3:0] wr_addr_r;
	logic [3:0] rd_addr_r;
	logic [47:0] rd_data_r;
	//logic [15:0] playback_l;
	logic [0:0] rd_valid_r;
	
	logic [47:0] mem [15:0];
	logic [3:0] last_written_r;
	
	always_ff @(posedge clk_i) begin
		//check if we need to toggle recording
		last_in_l <= data_i; // at end of frame, should have record of what we last saw, may need to dff this
		if (mode_toggle) begin
		//changing mode
			recording_l <= ~recording_l;
			if (recording_l) begin
				//entering playback mode
				rd_addr_r <= 4'b0000;
				rd_valid_r <= 1'b1;
				rd_data_r <= mem[4'b0000];
				counter <= 32'b00000000000000000000000000000000;
			end else begin
				//entering recording mode
				wr_addr_r <= 4'b0000; // reset write address
				counter <= 32'b00000000000000000000000000000000; // reset counter
				rd_valid_r <= 1'b0;
			end
		end else begin
			if (recording_l) begin
				//we are in recording mode
				if (last_in_l === data_i) begin
					//seeing same input as last time, increment counter and write it
					last_written_r <= wr_addr_r;
					mem[wr_addr_r] <= {last_in_l, counter};
					counter <= counter + 1'b1;
				end else begin
					//seeing something different, so need to advance write pointer
					//first check if doing so will put us out of bounds, a we will then need to exit record mode
					if (wr_addr_r == 4'b1110) begin
						//we are at the end of our recording limit, enter playback mode
						recording_l <= ~recording_l;
						rd_addr_r <= 4'b0000;
						rd_valid_r <= 1'b1;
						rd_data_r <= mem[4'b0000];
						wr_addr_r <= wr_addr_r + 1'b1;
						counter <= 32'b00000000000000000000000000000000;
					end else begin
						//still have room to record, reset counter and increment wr_addr_r
						wr_addr_r <= wr_addr_r + 1'b1;
						counter <= 32'b00000000000000000000000000000000;
					end
				end
			end else begin
			//we are in playback mode!
				if (counter >= rd_data_r[31:0]) begin
					//we have finished reading this entry need to advance the rd pointer
					//would doing so put us at the same pot as the wr pointer?
					if (rd_addr_r === last_written_r) begin
						//we need to loop around, set read back to 0
						rd_data_r <= mem[4'b0000];
						rd_addr_r <= 4'b0000;
						counter <= 32'b00000000000000000000000000000000;
					end else begin
						rd_data_r <= mem[(rd_addr_r + 1'b1)];
						rd_addr_r <= rd_addr_r + 1'b1;
						counter <= 32'b00000000000000000000000000000000;
					end
				end else begin
					//have not finished reading this entry just continue to increment the counter
					counter <= counter + 1'b1;
				end
			end
		end
	end

	assign data_o = rd_valid_r ? rd_data_r[47:32] : 16'b0000000000000000;
	assign recording_o = recording_l;
endmodule

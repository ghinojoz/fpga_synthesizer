// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtestbench.h for the primary calling header

#include "verilated.h"

#include "Vtestbench__Syms.h"
#include "Vtestbench___024root.h"

VL_ATTR_COLD void Vtestbench___024root___eval_initial__TOP(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial__TOP\n"); );
    // Body
    vlSelf->testbench__DOT__test_vector[0U] = 0U;
    vlSelf->testbench__DOT__test_vector[1U] = 0U;
    vlSelf->testbench__DOT__test_vector[2U] = 0U;
    vlSelf->testbench__DOT__test_vector[3U] = 0x10000U;
    vlSelf->testbench__DOT__test_vector[4U] = 0U;
    vlSelf->testbench__DOT__test_vector[5U] = 0U;
    vlSelf->testbench__DOT__test_vector[6U] = 0U;
    vlSelf->testbench__DOT__test_vector[7U] = 1U;
    vlSelf->testbench__DOT__test_vector[8U] = 1U;
    vlSelf->testbench__DOT__test_vector[9U] = 1U;
    vlSelf->testbench__DOT__test_vector[0xaU] = 1U;
    vlSelf->testbench__DOT__test_vector[0xbU] = 0U;
    vlSelf->testbench__DOT__test_vector[0xcU] = 0U;
    vlSelf->testbench__DOT__test_vector[0xdU] = 4U;
    vlSelf->testbench__DOT__test_vector[0xeU] = 4U;
    vlSelf->testbench__DOT__test_vector[0xfU] = 4U;
    vlSelf->testbench__DOT__test_vector[0x10U] = 4U;
    vlSelf->testbench__DOT__test_vector[0x11U] = 4U;
    vlSelf->testbench__DOT__test_vector[0x12U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x13U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x14U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x15U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x16U] = 0x10U;
    vlSelf->testbench__DOT__test_vector[0x17U] = 0x10U;
    vlSelf->testbench__DOT__test_vector[0x18U] = 0x10U;
    vlSelf->testbench__DOT__test_vector[0x19U] = 0x10U;
    vlSelf->testbench__DOT__test_vector[0x1aU] = 0x10U;
    vlSelf->testbench__DOT__test_vector[0x1bU] = 0x10U;
    vlSelf->testbench__DOT__test_vector[0x1cU] = 0x10U;
    vlSelf->testbench__DOT__test_vector[0x1dU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x1eU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x1fU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x20U] = 0x10000U;
    vlSelf->testbench__DOT__test_vector[0x21U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x22U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x23U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x24U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x25U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x26U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x27U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x28U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x29U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x2aU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x2bU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x2cU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x2dU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x2eU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x2fU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x30U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x31U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x32U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x33U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x34U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x35U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x36U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x37U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x38U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x39U] = 0U;
    vlSelf->testbench__DOT__test_vector[0x3aU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x3bU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x3cU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x3dU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x3eU] = 0U;
    vlSelf->testbench__DOT__test_vector[0x3fU] = 0U;
    VL_WRITEF("%Ntestbench.cg with cycle_time_p          10\n",
              vlSymsp->name());
}

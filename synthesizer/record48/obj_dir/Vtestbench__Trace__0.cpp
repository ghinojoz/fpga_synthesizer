// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_fst_c.h"
#include "Vtestbench__Syms.h"


void Vtestbench___024root__trace_chg_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp);

void Vtestbench___024root__trace_chg_top_0(void* voidSelf, VerilatedFst::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_chg_top_0\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    Vtestbench___024root__trace_chg_sub_0((&vlSymsp->TOP), bufp);
}

void Vtestbench___024root__trace_chg_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_chg_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode + 1);
    // Body
    if (VL_UNLIKELY(vlSelf->__Vm_traceActivity[1U])) {
        bufp->chgSData(oldp+0,(((IData)(vlSelf->testbench__DOT__dut__DOT__rd_valid_r)
                                 ? (0xffffU & (IData)(
                                                      (vlSelf->testbench__DOT__dut__DOT__rd_data_r 
                                                       >> 0x20U)))
                                 : 0U)),16);
        bufp->chgBit(oldp+1,(vlSelf->testbench__DOT__dut__DOT__recording_l));
        bufp->chgSData(oldp+2,(vlSelf->testbench__DOT__dut__DOT__last_in_l),16);
        bufp->chgIData(oldp+3,(vlSelf->testbench__DOT__dut__DOT__counter),32);
        bufp->chgCData(oldp+4,(vlSelf->testbench__DOT__dut__DOT__wr_addr_r),4);
        bufp->chgCData(oldp+5,(vlSelf->testbench__DOT__dut__DOT__rd_addr_r),4);
        bufp->chgQData(oldp+6,(vlSelf->testbench__DOT__dut__DOT__rd_data_r),48);
        bufp->chgBit(oldp+8,(vlSelf->testbench__DOT__dut__DOT__rd_valid_r));
        bufp->chgQData(oldp+9,(vlSelf->testbench__DOT__dut__DOT__mem[0]),48);
        bufp->chgQData(oldp+11,(vlSelf->testbench__DOT__dut__DOT__mem[1]),48);
        bufp->chgQData(oldp+13,(vlSelf->testbench__DOT__dut__DOT__mem[2]),48);
        bufp->chgQData(oldp+15,(vlSelf->testbench__DOT__dut__DOT__mem[3]),48);
        bufp->chgQData(oldp+17,(vlSelf->testbench__DOT__dut__DOT__mem[4]),48);
        bufp->chgQData(oldp+19,(vlSelf->testbench__DOT__dut__DOT__mem[5]),48);
        bufp->chgQData(oldp+21,(vlSelf->testbench__DOT__dut__DOT__mem[6]),48);
        bufp->chgQData(oldp+23,(vlSelf->testbench__DOT__dut__DOT__mem[7]),48);
        bufp->chgQData(oldp+25,(vlSelf->testbench__DOT__dut__DOT__mem[8]),48);
        bufp->chgQData(oldp+27,(vlSelf->testbench__DOT__dut__DOT__mem[9]),48);
        bufp->chgQData(oldp+29,(vlSelf->testbench__DOT__dut__DOT__mem[10]),48);
        bufp->chgQData(oldp+31,(vlSelf->testbench__DOT__dut__DOT__mem[11]),48);
        bufp->chgQData(oldp+33,(vlSelf->testbench__DOT__dut__DOT__mem[12]),48);
        bufp->chgQData(oldp+35,(vlSelf->testbench__DOT__dut__DOT__mem[13]),48);
        bufp->chgQData(oldp+37,(vlSelf->testbench__DOT__dut__DOT__mem[14]),48);
        bufp->chgQData(oldp+39,(vlSelf->testbench__DOT__dut__DOT__mem[15]),48);
        bufp->chgCData(oldp+41,(vlSelf->testbench__DOT__dut__DOT__last_written_r),4);
    }
    bufp->chgBit(oldp+42,(vlSelf->testbench__DOT__clk_i));
    bufp->chgBit(oldp+43,(vlSelf->testbench__DOT__mode_toggle_i));
    bufp->chgSData(oldp+44,(vlSelf->testbench__DOT__data_i),16);
    bufp->chgIData(oldp+45,(vlSelf->testbench__DOT__itervar),32);
}

void Vtestbench___024root__trace_cleanup(void* voidSelf, VerilatedFst* /*unused*/) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_cleanup\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    vlSymsp->__Vm_activity = false;
    vlSymsp->TOP.__Vm_traceActivity[0U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[1U] = 0U;
}

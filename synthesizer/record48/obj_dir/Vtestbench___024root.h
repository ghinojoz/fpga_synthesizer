// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vtestbench.h for the primary calling header

#ifndef VERILATED_VTESTBENCH___024ROOT_H_
#define VERILATED_VTESTBENCH___024ROOT_H_  // guard

#include "verilated.h"
#include "verilated_timing.h"

class Vtestbench__Syms;

class Vtestbench___024root final : public VerilatedModule {
  public:

    // DESIGN SPECIFIC STATE
    CData/*0:0*/ testbench__DOT__clk_i;
    CData/*0:0*/ testbench__DOT__mode_toggle_i;
    CData/*0:0*/ testbench__DOT__dut__DOT__recording_l;
    CData/*3:0*/ testbench__DOT__dut__DOT__wr_addr_r;
    CData/*3:0*/ testbench__DOT__dut__DOT__rd_addr_r;
    CData/*0:0*/ testbench__DOT__dut__DOT__rd_valid_r;
    CData/*3:0*/ testbench__DOT__dut__DOT__last_written_r;
    CData/*0:0*/ __Vtrigrprev__TOP__testbench__DOT__clk_i;
    CData/*0:0*/ __VactContinue;
    SData/*15:0*/ testbench__DOT__data_i;
    SData/*15:0*/ testbench__DOT__dut__DOT__last_in_l;
    IData/*31:0*/ testbench__DOT__itervar;
    IData/*31:0*/ testbench__DOT__dut__DOT__counter;
    IData/*31:0*/ __VactIterCount;
    QData/*47:0*/ testbench__DOT__dut__DOT__rd_data_r;
    VlUnpacked<IData/*16:0*/, 64> testbench__DOT__test_vector;
    VlUnpacked<QData/*47:0*/, 16> testbench__DOT__dut__DOT__mem;
    VlUnpacked<CData/*0:0*/, 2> __Vm_traceActivity;
    VlDelayScheduler __VdlySched;
    VlTriggerScheduler __VtrigSched_hef748430__0;
    VlTriggerVec<3> __VactTriggered;
    VlTriggerVec<3> __VnbaTriggered;

    // INTERNAL VARIABLES
    Vtestbench__Syms* const vlSymsp;

    // CONSTRUCTORS
    Vtestbench___024root(Vtestbench__Syms* symsp, const char* name);
    ~Vtestbench___024root();
    VL_UNCOPYABLE(Vtestbench___024root);

    // INTERNAL METHODS
    void __Vconfigure(bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard

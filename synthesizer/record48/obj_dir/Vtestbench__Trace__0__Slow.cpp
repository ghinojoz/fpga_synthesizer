// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_fst_c.h"
#include "Vtestbench__Syms.h"


VL_ATTR_COLD void Vtestbench___024root__trace_init_sub__TOP__0(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_init_sub__TOP__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->pushNamePrefix("testbench ");
    tracep->declBus(c+43,"clk_i",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+44,"mode_toggle_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+45,"data_i",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 15,0);
    tracep->declBus(c+1,"data_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 15,0);
    tracep->declBus(c+2,"recording_o",-1, FST_VD_IMPLICIT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+46,"itervar",-1, FST_VD_IMPLICIT,FST_VT_SV_INT, false,-1, 31,0);
    tracep->pushNamePrefix("cg ");
    tracep->declBus(c+47,"cycle_time_p",-1, FST_VD_IMPLICIT,FST_VT_VCD_PARAMETER, false,-1, 31,0);
    tracep->declBit(c+43,"clk_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("dut ");
    tracep->declBus(c+43,"clk_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+44,"mode_toggle",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+45,"data_i",-1,FST_VD_INPUT,FST_VT_VCD_WIRE, false,-1, 15,0);
    tracep->declBus(c+1,"data_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 15,0);
    tracep->declBus(c+2,"recording_o",-1,FST_VD_OUTPUT,FST_VT_VCD_WIRE, false,-1, 0,0);
    tracep->declBus(c+2,"recording_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    tracep->declBus(c+3,"last_in_l",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 15,0);
    tracep->declBus(c+4,"counter",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 31,0);
    tracep->declBus(c+5,"wr_addr_r",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 3,0);
    tracep->declBus(c+6,"rd_addr_r",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 3,0);
    tracep->declQuad(c+7,"rd_data_r",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 47,0);
    tracep->declBus(c+9,"rd_valid_r",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 0,0);
    for (int i = 0; i < 16; ++i) {
        tracep->declQuad(c+10+i*2,"mem",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, true,(i+0), 47,0);
    }
    tracep->declBus(c+42,"last_written_r",-1, FST_VD_IMPLICIT,FST_VT_SV_LOGIC, false,-1, 3,0);
    tracep->popNamePrefix(2);
}

VL_ATTR_COLD void Vtestbench___024root__trace_init_top(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_init_top\n"); );
    // Body
    Vtestbench___024root__trace_init_sub__TOP__0(vlSelf, tracep);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_top_0(void* voidSelf, VerilatedFst::Buffer* bufp);
void Vtestbench___024root__trace_chg_top_0(void* voidSelf, VerilatedFst::Buffer* bufp);
void Vtestbench___024root__trace_cleanup(void* voidSelf, VerilatedFst* /*unused*/);

VL_ATTR_COLD void Vtestbench___024root__trace_register(Vtestbench___024root* vlSelf, VerilatedFst* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_register\n"); );
    // Body
    tracep->addFullCb(&Vtestbench___024root__trace_full_top_0, vlSelf);
    tracep->addChgCb(&Vtestbench___024root__trace_chg_top_0, vlSelf);
    tracep->addCleanupCb(&Vtestbench___024root__trace_cleanup, vlSelf);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp);

VL_ATTR_COLD void Vtestbench___024root__trace_full_top_0(void* voidSelf, VerilatedFst::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_full_top_0\n"); );
    // Init
    Vtestbench___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<Vtestbench___024root*>(voidSelf);
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    Vtestbench___024root__trace_full_sub_0((&vlSymsp->TOP), bufp);
}

VL_ATTR_COLD void Vtestbench___024root__trace_full_sub_0(Vtestbench___024root* vlSelf, VerilatedFst::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root__trace_full_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode);
    // Body
    bufp->fullSData(oldp+1,(((IData)(vlSelf->testbench__DOT__dut__DOT__rd_valid_r)
                              ? (0xffffU & (IData)(
                                                   (vlSelf->testbench__DOT__dut__DOT__rd_data_r 
                                                    >> 0x20U)))
                              : 0U)),16);
    bufp->fullBit(oldp+2,(vlSelf->testbench__DOT__dut__DOT__recording_l));
    bufp->fullSData(oldp+3,(vlSelf->testbench__DOT__dut__DOT__last_in_l),16);
    bufp->fullIData(oldp+4,(vlSelf->testbench__DOT__dut__DOT__counter),32);
    bufp->fullCData(oldp+5,(vlSelf->testbench__DOT__dut__DOT__wr_addr_r),4);
    bufp->fullCData(oldp+6,(vlSelf->testbench__DOT__dut__DOT__rd_addr_r),4);
    bufp->fullQData(oldp+7,(vlSelf->testbench__DOT__dut__DOT__rd_data_r),48);
    bufp->fullBit(oldp+9,(vlSelf->testbench__DOT__dut__DOT__rd_valid_r));
    bufp->fullQData(oldp+10,(vlSelf->testbench__DOT__dut__DOT__mem[0]),48);
    bufp->fullQData(oldp+12,(vlSelf->testbench__DOT__dut__DOT__mem[1]),48);
    bufp->fullQData(oldp+14,(vlSelf->testbench__DOT__dut__DOT__mem[2]),48);
    bufp->fullQData(oldp+16,(vlSelf->testbench__DOT__dut__DOT__mem[3]),48);
    bufp->fullQData(oldp+18,(vlSelf->testbench__DOT__dut__DOT__mem[4]),48);
    bufp->fullQData(oldp+20,(vlSelf->testbench__DOT__dut__DOT__mem[5]),48);
    bufp->fullQData(oldp+22,(vlSelf->testbench__DOT__dut__DOT__mem[6]),48);
    bufp->fullQData(oldp+24,(vlSelf->testbench__DOT__dut__DOT__mem[7]),48);
    bufp->fullQData(oldp+26,(vlSelf->testbench__DOT__dut__DOT__mem[8]),48);
    bufp->fullQData(oldp+28,(vlSelf->testbench__DOT__dut__DOT__mem[9]),48);
    bufp->fullQData(oldp+30,(vlSelf->testbench__DOT__dut__DOT__mem[10]),48);
    bufp->fullQData(oldp+32,(vlSelf->testbench__DOT__dut__DOT__mem[11]),48);
    bufp->fullQData(oldp+34,(vlSelf->testbench__DOT__dut__DOT__mem[12]),48);
    bufp->fullQData(oldp+36,(vlSelf->testbench__DOT__dut__DOT__mem[13]),48);
    bufp->fullQData(oldp+38,(vlSelf->testbench__DOT__dut__DOT__mem[14]),48);
    bufp->fullQData(oldp+40,(vlSelf->testbench__DOT__dut__DOT__mem[15]),48);
    bufp->fullCData(oldp+42,(vlSelf->testbench__DOT__dut__DOT__last_written_r),4);
    bufp->fullBit(oldp+43,(vlSelf->testbench__DOT__clk_i));
    bufp->fullBit(oldp+44,(vlSelf->testbench__DOT__mode_toggle_i));
    bufp->fullSData(oldp+45,(vlSelf->testbench__DOT__data_i),16);
    bufp->fullIData(oldp+46,(vlSelf->testbench__DOT__itervar),32);
    bufp->fullIData(oldp+47,(0xaU),32);
}

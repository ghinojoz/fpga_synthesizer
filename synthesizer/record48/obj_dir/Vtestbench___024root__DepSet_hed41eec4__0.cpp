// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtestbench.h for the primary calling header

#include "verilated.h"

#include "Vtestbench___024root.h"

VL_ATTR_COLD void Vtestbench___024root___eval_initial__TOP(Vtestbench___024root* vlSelf);
VlCoroutine Vtestbench___024root___eval_initial__TOP__0(Vtestbench___024root* vlSelf);
VlCoroutine Vtestbench___024root___eval_initial__TOP__1(Vtestbench___024root* vlSelf);

void Vtestbench___024root___eval_initial(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial\n"); );
    // Body
    Vtestbench___024root___eval_initial__TOP(vlSelf);
    Vtestbench___024root___eval_initial__TOP__0(vlSelf);
    Vtestbench___024root___eval_initial__TOP__1(vlSelf);
    vlSelf->__Vtrigrprev__TOP__testbench__DOT__clk_i 
        = vlSelf->testbench__DOT__clk_i;
}

VL_INLINE_OPT VlCoroutine Vtestbench___024root___eval_initial__TOP__1(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_initial__TOP__1\n"); );
    // Body
    while (1U) {
        co_await vlSelf->__VdlySched.delay(0x1388U, 
                                           "../../provided_modules/nonsynth_clock_gen.sv", 
                                           11);
        vlSelf->testbench__DOT__clk_i = (1U & (~ (IData)(vlSelf->testbench__DOT__clk_i)));
    }
}

void Vtestbench___024root___eval_act(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_act\n"); );
}

VL_INLINE_OPT void Vtestbench___024root___nba_sequent__TOP__0(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___nba_sequent__TOP__0\n"); );
    // Init
    CData/*0:0*/ __Vdly__testbench__DOT__dut__DOT__recording_l;
    __Vdly__testbench__DOT__dut__DOT__recording_l = 0;
    CData/*3:0*/ __Vdly__testbench__DOT__dut__DOT__rd_addr_r;
    __Vdly__testbench__DOT__dut__DOT__rd_addr_r = 0;
    QData/*47:0*/ __Vdly__testbench__DOT__dut__DOT__rd_data_r;
    __Vdly__testbench__DOT__dut__DOT__rd_data_r = 0;
    IData/*31:0*/ __Vdly__testbench__DOT__dut__DOT__counter;
    __Vdly__testbench__DOT__dut__DOT__counter = 0;
    CData/*3:0*/ __Vdly__testbench__DOT__dut__DOT__wr_addr_r;
    __Vdly__testbench__DOT__dut__DOT__wr_addr_r = 0;
    CData/*3:0*/ __Vdly__testbench__DOT__dut__DOT__last_written_r;
    __Vdly__testbench__DOT__dut__DOT__last_written_r = 0;
    CData/*3:0*/ __Vdlyvdim0__testbench__DOT__dut__DOT__mem__v0;
    __Vdlyvdim0__testbench__DOT__dut__DOT__mem__v0 = 0;
    QData/*47:0*/ __Vdlyvval__testbench__DOT__dut__DOT__mem__v0;
    __Vdlyvval__testbench__DOT__dut__DOT__mem__v0 = 0;
    CData/*0:0*/ __Vdlyvset__testbench__DOT__dut__DOT__mem__v0;
    __Vdlyvset__testbench__DOT__dut__DOT__mem__v0 = 0;
    // Body
    __Vdly__testbench__DOT__dut__DOT__last_written_r 
        = vlSelf->testbench__DOT__dut__DOT__last_written_r;
    __Vdly__testbench__DOT__dut__DOT__wr_addr_r = vlSelf->testbench__DOT__dut__DOT__wr_addr_r;
    __Vdly__testbench__DOT__dut__DOT__counter = vlSelf->testbench__DOT__dut__DOT__counter;
    __Vdly__testbench__DOT__dut__DOT__rd_data_r = vlSelf->testbench__DOT__dut__DOT__rd_data_r;
    __Vdly__testbench__DOT__dut__DOT__rd_addr_r = vlSelf->testbench__DOT__dut__DOT__rd_addr_r;
    __Vdly__testbench__DOT__dut__DOT__recording_l = vlSelf->testbench__DOT__dut__DOT__recording_l;
    __Vdlyvset__testbench__DOT__dut__DOT__mem__v0 = 0U;
    if (vlSelf->testbench__DOT__mode_toggle_i) {
        __Vdly__testbench__DOT__dut__DOT__recording_l 
            = (1U & (~ (IData)(vlSelf->testbench__DOT__dut__DOT__recording_l)));
        if (vlSelf->testbench__DOT__dut__DOT__recording_l) {
            __Vdly__testbench__DOT__dut__DOT__rd_addr_r = 0U;
            vlSelf->testbench__DOT__dut__DOT__rd_valid_r = 1U;
            __Vdly__testbench__DOT__dut__DOT__rd_data_r 
                = vlSelf->testbench__DOT__dut__DOT__mem
                [0U];
            __Vdly__testbench__DOT__dut__DOT__counter = 0U;
        } else {
            __Vdly__testbench__DOT__dut__DOT__wr_addr_r = 0U;
            __Vdly__testbench__DOT__dut__DOT__counter = 0U;
            vlSelf->testbench__DOT__dut__DOT__rd_valid_r = 0U;
        }
    } else if (vlSelf->testbench__DOT__dut__DOT__recording_l) {
        if (((IData)(vlSelf->testbench__DOT__dut__DOT__last_in_l) 
             == (IData)(vlSelf->testbench__DOT__data_i))) {
            __Vdly__testbench__DOT__dut__DOT__last_written_r 
                = vlSelf->testbench__DOT__dut__DOT__wr_addr_r;
            __Vdlyvval__testbench__DOT__dut__DOT__mem__v0 
                = (((QData)((IData)(vlSelf->testbench__DOT__dut__DOT__last_in_l)) 
                    << 0x20U) | (QData)((IData)(vlSelf->testbench__DOT__dut__DOT__counter)));
            __Vdlyvset__testbench__DOT__dut__DOT__mem__v0 = 1U;
            __Vdlyvdim0__testbench__DOT__dut__DOT__mem__v0 
                = vlSelf->testbench__DOT__dut__DOT__wr_addr_r;
            __Vdly__testbench__DOT__dut__DOT__counter 
                = ((IData)(1U) + vlSelf->testbench__DOT__dut__DOT__counter);
        } else if ((0xeU == (IData)(vlSelf->testbench__DOT__dut__DOT__wr_addr_r))) {
            __Vdly__testbench__DOT__dut__DOT__recording_l 
                = (1U & (~ (IData)(vlSelf->testbench__DOT__dut__DOT__recording_l)));
            __Vdly__testbench__DOT__dut__DOT__wr_addr_r 
                = (0xfU & ((IData)(1U) + (IData)(vlSelf->testbench__DOT__dut__DOT__wr_addr_r)));
            __Vdly__testbench__DOT__dut__DOT__rd_addr_r = 0U;
            vlSelf->testbench__DOT__dut__DOT__rd_valid_r = 1U;
            __Vdly__testbench__DOT__dut__DOT__rd_data_r 
                = vlSelf->testbench__DOT__dut__DOT__mem
                [0U];
            __Vdly__testbench__DOT__dut__DOT__counter = 0U;
        } else {
            __Vdly__testbench__DOT__dut__DOT__wr_addr_r 
                = (0xfU & ((IData)(1U) + (IData)(vlSelf->testbench__DOT__dut__DOT__wr_addr_r)));
            __Vdly__testbench__DOT__dut__DOT__counter = 0U;
        }
    } else if ((vlSelf->testbench__DOT__dut__DOT__counter 
                >= (IData)(vlSelf->testbench__DOT__dut__DOT__rd_data_r))) {
        if (((IData)(vlSelf->testbench__DOT__dut__DOT__rd_addr_r) 
             == (IData)(vlSelf->testbench__DOT__dut__DOT__last_written_r))) {
            __Vdly__testbench__DOT__dut__DOT__rd_data_r 
                = vlSelf->testbench__DOT__dut__DOT__mem
                [0U];
            __Vdly__testbench__DOT__dut__DOT__rd_addr_r = 0U;
            __Vdly__testbench__DOT__dut__DOT__counter = 0U;
        } else {
            __Vdly__testbench__DOT__dut__DOT__rd_data_r 
                = vlSelf->testbench__DOT__dut__DOT__mem
                [(0xfU & ((IData)(1U) + (IData)(vlSelf->testbench__DOT__dut__DOT__rd_addr_r)))];
            __Vdly__testbench__DOT__dut__DOT__counter = 0U;
            __Vdly__testbench__DOT__dut__DOT__rd_addr_r 
                = (0xfU & ((IData)(1U) + (IData)(vlSelf->testbench__DOT__dut__DOT__rd_addr_r)));
        }
    } else {
        __Vdly__testbench__DOT__dut__DOT__counter = 
            ((IData)(1U) + vlSelf->testbench__DOT__dut__DOT__counter);
    }
    vlSelf->testbench__DOT__dut__DOT__recording_l = __Vdly__testbench__DOT__dut__DOT__recording_l;
    vlSelf->testbench__DOT__dut__DOT__rd_addr_r = __Vdly__testbench__DOT__dut__DOT__rd_addr_r;
    vlSelf->testbench__DOT__dut__DOT__rd_data_r = __Vdly__testbench__DOT__dut__DOT__rd_data_r;
    vlSelf->testbench__DOT__dut__DOT__counter = __Vdly__testbench__DOT__dut__DOT__counter;
    vlSelf->testbench__DOT__dut__DOT__wr_addr_r = __Vdly__testbench__DOT__dut__DOT__wr_addr_r;
    vlSelf->testbench__DOT__dut__DOT__last_written_r 
        = __Vdly__testbench__DOT__dut__DOT__last_written_r;
    if (__Vdlyvset__testbench__DOT__dut__DOT__mem__v0) {
        vlSelf->testbench__DOT__dut__DOT__mem[__Vdlyvdim0__testbench__DOT__dut__DOT__mem__v0] 
            = __Vdlyvval__testbench__DOT__dut__DOT__mem__v0;
    }
    vlSelf->testbench__DOT__dut__DOT__last_in_l = vlSelf->testbench__DOT__data_i;
}

VL_INLINE_OPT void Vtestbench___024root___nba_sequent__TOP__1(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___nba_sequent__TOP__1\n"); );
    // Body
    vlSelf->testbench__DOT__mode_toggle_i = (1U & (
                                                   vlSelf->testbench__DOT__test_vector
                                                   [
                                                   (0x3fU 
                                                    & vlSelf->testbench__DOT__itervar)] 
                                                   >> 0x10U));
    vlSelf->testbench__DOT__data_i = (0xffffU & vlSelf->testbench__DOT__test_vector
                                      [(0x3fU & vlSelf->testbench__DOT__itervar)]);
}

void Vtestbench___024root___eval_nba(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_nba\n"); );
    // Body
    if (vlSelf->__VnbaTriggered.at(1U)) {
        Vtestbench___024root___nba_sequent__TOP__0(vlSelf);
        vlSelf->__Vm_traceActivity[1U] = 1U;
    }
    if (vlSelf->__VnbaTriggered.at(0U)) {
        Vtestbench___024root___nba_sequent__TOP__1(vlSelf);
    }
}

void Vtestbench___024root___eval_triggers__act(Vtestbench___024root* vlSelf);
void Vtestbench___024root___timing_commit(Vtestbench___024root* vlSelf);
#ifdef VL_DEBUG
VL_ATTR_COLD void Vtestbench___024root___dump_triggers__act(Vtestbench___024root* vlSelf);
#endif  // VL_DEBUG
void Vtestbench___024root___timing_resume(Vtestbench___024root* vlSelf);
#ifdef VL_DEBUG
VL_ATTR_COLD void Vtestbench___024root___dump_triggers__nba(Vtestbench___024root* vlSelf);
#endif  // VL_DEBUG

void Vtestbench___024root___eval(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval\n"); );
    // Init
    VlTriggerVec<3> __VpreTriggered;
    IData/*31:0*/ __VnbaIterCount;
    CData/*0:0*/ __VnbaContinue;
    // Body
    __VnbaIterCount = 0U;
    __VnbaContinue = 1U;
    while (__VnbaContinue) {
        __VnbaContinue = 0U;
        vlSelf->__VnbaTriggered.clear();
        vlSelf->__VactIterCount = 0U;
        vlSelf->__VactContinue = 1U;
        while (vlSelf->__VactContinue) {
            vlSelf->__VactContinue = 0U;
            Vtestbench___024root___eval_triggers__act(vlSelf);
            Vtestbench___024root___timing_commit(vlSelf);
            if (vlSelf->__VactTriggered.any()) {
                vlSelf->__VactContinue = 1U;
                if (VL_UNLIKELY((0x64U < vlSelf->__VactIterCount))) {
#ifdef VL_DEBUG
                    Vtestbench___024root___dump_triggers__act(vlSelf);
#endif
                    VL_FATAL_MT("testbench.sv", 2, "", "Active region did not converge.");
                }
                vlSelf->__VactIterCount = ((IData)(1U) 
                                           + vlSelf->__VactIterCount);
                __VpreTriggered.andNot(vlSelf->__VactTriggered, vlSelf->__VnbaTriggered);
                vlSelf->__VnbaTriggered.set(vlSelf->__VactTriggered);
                Vtestbench___024root___timing_resume(vlSelf);
                Vtestbench___024root___eval_act(vlSelf);
            }
        }
        if (vlSelf->__VnbaTriggered.any()) {
            __VnbaContinue = 1U;
            if (VL_UNLIKELY((0x64U < __VnbaIterCount))) {
#ifdef VL_DEBUG
                Vtestbench___024root___dump_triggers__nba(vlSelf);
#endif
                VL_FATAL_MT("testbench.sv", 2, "", "NBA region did not converge.");
            }
            __VnbaIterCount = ((IData)(1U) + __VnbaIterCount);
            Vtestbench___024root___eval_nba(vlSelf);
        }
    }
}

void Vtestbench___024root___timing_commit(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___timing_commit\n"); );
    // Body
    if ((1U & (~ (IData)(vlSelf->__VactTriggered.at(1U))))) {
        vlSelf->__VtrigSched_hef748430__0.commit("@(posedge testbench.clk_i)");
    }
}

void Vtestbench___024root___timing_resume(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___timing_resume\n"); );
    // Body
    if (vlSelf->__VactTriggered.at(2U)) {
        vlSelf->__VdlySched.resume();
    }
    if (vlSelf->__VactTriggered.at(1U)) {
        vlSelf->__VtrigSched_hef748430__0.resume("@(posedge testbench.clk_i)");
    }
}

#ifdef VL_DEBUG
void Vtestbench___024root___eval_debug_assertions(Vtestbench___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    Vtestbench__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtestbench___024root___eval_debug_assertions\n"); );
}
#endif  // VL_DEBUG
